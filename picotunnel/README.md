# Cubelab Pico MCU Tunnel App

### Authors:

Galen Nare

---

A CLI application to facilitate communication between the computer's REST API and
the Raspberry Pi Pico MCU inside the CubeLab.

### Top-level dependencies:

- _JSerialComm_ - Library to enable serial port communication.
- _MapStruct_ - Used by the data model classes to easily map between Hibernate Entity
  and DTO objects.
- _SLF4J and Logback_ - Logging interface to enable pretty-printed messages and various log levels.

### How to run/install:

- Clone the repo using `git clone`
- Ensure Maven is installed (version 3.8 is preferred).
- Ensure the Pico Data models project has already been installed (../datamodels).
- To run the app, run `mvn clean compile exec:java`.
- To install to the local repo and build a JAR, run `mvn clean install`

### Connection Info:

 - Make sure the `rest-api` is running on port `8080`.

### License:

MIT

Feel free to use this code for whatever you want (credit appreciated but not required), 
but if you claim you wrote it I will publicly shame you :)