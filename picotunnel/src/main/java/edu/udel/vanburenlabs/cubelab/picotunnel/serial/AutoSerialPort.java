/*
 * MIT License
 *
 * Copyright (c) 2024 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.picotunnel.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Wrapper class for {@link SerialPort}. Enables {@link Closeable functionality} and
 * adds functions for String pre-processing when reading and writing to the port.
 * <p></p>
 * Has to wrap the class instead of extend it because {@link SerialPort} is <code>final</code> for some reason :(
 * <p></p>
 * See the method docs for that class in the case of the wrapped methods.
 */
@Slf4j
@SuppressWarnings({ "unused", "UnusedReturnValue" })
public class AutoSerialPort implements Closeable {
    private final SerialPort serialPort;

    public AutoSerialPort(SerialPort serialPort) {
        this.serialPort = serialPort;
    }

    public String toString() {
        return serialPort.toString();
    }

    public synchronized boolean openPort(int safetySleepTime, int deviceSendQueueSize, int deviceReceiveQueueSize) {
        return serialPort.openPort(safetySleepTime, deviceSendQueueSize, deviceReceiveQueueSize);
    }

    public boolean openPort(int safetySleepTime) {
        return serialPort.openPort(safetySleepTime);
    }

    public boolean openPort() {
        return serialPort.openPort();
    }

    public synchronized boolean closePort() {
        return serialPort.closePort();
    }

    public synchronized boolean isOpen() {
        return serialPort.isOpen();
    }

    public synchronized void disablePortConfiguration() {
        serialPort.disablePortConfiguration();
    }

    public synchronized void disableExclusiveLock() {
        serialPort.disableExclusiveLock();
    }

    public synchronized void allowElevatedPermissionsRequest() {
        serialPort.allowElevatedPermissionsRequest();
    }

    public synchronized int getLastErrorLocation() {
        return serialPort.getLastErrorLocation();
    }

    public synchronized int getLastErrorCode() {
        return serialPort.getLastErrorCode();
    }

    public int bytesAvailable() {
        return serialPort.bytesAvailable();
    }

    public int bytesAwaitingWrite() {
        return serialPort.bytesAwaitingWrite();
    }

    public int readBytes(byte[] buffer, long bytesToRead) {
        return serialPort.readBytes(buffer, bytesToRead);
    }

    public int readBytes(byte[] buffer, long bytesToRead, long offset) {
        return serialPort.readBytes(buffer, bytesToRead, offset);
    }

    public int writeBytes(byte[] buffer, long bytesToWrite) {
        return serialPort.writeBytes(buffer, bytesToWrite);
    }

    public int writeBytes(byte[] buffer, long bytesToWrite, long offset) {
        return serialPort.writeBytes(buffer, bytesToWrite, offset);
    }

    public int getDeviceWriteBufferSize() {
        return serialPort.getDeviceWriteBufferSize();
    }

    public int getDeviceReadBufferSize() {
        return serialPort.getDeviceReadBufferSize();
    }

    public boolean setBreak() {
        return serialPort.setBreak();
    }

    public boolean clearBreak() {
        return serialPort.clearBreak();
    }

    public boolean setRTS() {
        return serialPort.setRTS();
    }

    public boolean clearRTS() {
        return serialPort.clearRTS();
    }

    public boolean setDTR() {
        return serialPort.setDTR();
    }

    public boolean clearDTR() {
        return serialPort.clearDTR();
    }

    public boolean getCTS() {
        return serialPort.getCTS();
    }

    public boolean getDSR() {
        return serialPort.getDSR();
    }

    public boolean getDCD() {
        return serialPort.getDCD();
    }

    public boolean getDTR() {
        return serialPort.getDTR();
    }

    public boolean getRTS() {
        return serialPort.getRTS();
    }

    public boolean getRI() {
        return serialPort.getRI();
    }

    public synchronized boolean addDataListener(SerialPortDataListener listener) {
        return serialPort.addDataListener(listener);
    }

    public synchronized void removeDataListener() {
        serialPort.removeDataListener();
    }

    public InputStream getInputStream() {
        return serialPort.getInputStream();
    }

    public InputStream getInputStreamWithSuppressedTimeoutExceptions() {
        return serialPort.getInputStreamWithSuppressedTimeoutExceptions();
    }

    public OutputStream getOutputStream() {
        return serialPort.getOutputStream();
    }

    public synchronized boolean flushIOBuffers() {
        return serialPort.flushIOBuffers();
    }

    public boolean setComPortParameters(int newBaudRate, int newDataBits, int newStopBits, int newParity) {
        return serialPort.setComPortParameters(newBaudRate, newDataBits, newStopBits, newParity);
    }

    public synchronized boolean setComPortParameters(int newBaudRate, int newDataBits, int newStopBits, int newParity, boolean useRS485Mode) {
        return serialPort.setComPortParameters(newBaudRate, newDataBits, newStopBits, newParity, useRS485Mode);
    }

    public synchronized boolean setComPortTimeouts(int newTimeoutMode, int newReadTimeout, int newWriteTimeout) {
        return serialPort.setComPortTimeouts(newTimeoutMode, newReadTimeout, newWriteTimeout);
    }

    public synchronized boolean setBaudRate(int newBaudRate) {
        return serialPort.setBaudRate(newBaudRate);
    }

    public synchronized boolean setNumDataBits(int newDataBits) {
        return serialPort.setNumDataBits(newDataBits);
    }

    public synchronized boolean setNumStopBits(int newStopBits) {
        return serialPort.setNumStopBits(newStopBits);
    }

    public synchronized boolean setFlowControl(int newFlowControlSettings) {
        return serialPort.setFlowControl(newFlowControlSettings);
    }

    public synchronized boolean setParity(int newParity) {
        return serialPort.setParity(newParity);
    }

    public synchronized boolean setRs485ModeParameters(boolean useRS485Mode, boolean rs485RtsActiveHigh, int delayBeforeSendMicroseconds, int delayAfterSendMicroseconds) {
        return serialPort.setRs485ModeParameters(useRS485Mode, rs485RtsActiveHigh, delayBeforeSendMicroseconds, delayAfterSendMicroseconds);
    }

    public synchronized boolean setRs485ModeParameters(boolean useRS485Mode, boolean rs485RtsActiveHigh, boolean enableTermination, boolean rxDuringTx, int delayBeforeSendMicroseconds, int delayAfterSendMicroseconds) {
        return serialPort.setRs485ModeParameters(useRS485Mode, rs485RtsActiveHigh, enableTermination, rxDuringTx, delayBeforeSendMicroseconds, delayAfterSendMicroseconds);
    }

    public synchronized boolean setXonXoffCharacters(byte xonStartCharacter, byte xoffStopCharacter) {
        return serialPort.setXonXoffCharacters(xonStartCharacter, xoffStopCharacter);
    }

    public String getDescriptivePortName() {
        return serialPort.getDescriptivePortName();
    }

    public String getSystemPortName() {
        return serialPort.getSystemPortName();
    }

    public String getSystemPortPath() {
        return serialPort.getSystemPortPath();
    }

    public String getPortDescription() {
        return serialPort.getPortDescription();
    }

    public String getPortLocation() {
        return serialPort.getPortLocation();
    }

    public int getBaudRate() {
        return serialPort.getBaudRate();
    }

    public int getNumDataBits() {
        return serialPort.getNumDataBits();
    }

    public int getNumStopBits() {
        return serialPort.getNumStopBits();
    }

    public int getParity() {
        return serialPort.getParity();
    }

    public int getReadTimeout() {
        return serialPort.getReadTimeout();
    }

    public int getWriteTimeout() {
        return serialPort.getWriteTimeout();
    }

    public int getFlowControlSettings() {
        return serialPort.getFlowControlSettings();
    }

    /**
     * Writes a string to the serial port (and logs it to the debug log if enabled).
     * @param s the string to write
     * @return the number of bytes written
     */
    public int writeString(String s) {
        int len = s.length();
        log.debug("-> {}: {}", serialPort.getSystemPortPath(), s.trim());
        return writeBytes(s.getBytes(StandardCharsets.UTF_8), len);
    }

    public String readString(int bytesToRead) {
        ByteBuffer buffer = ByteBuffer.allocate(bytesToRead);
        readBytes(buffer.array(), bytesToRead);
        return buffer.asCharBuffer().rewind().toString().trim();
    }

    /**
     * Reads chars from the serial port until the terminator char is reached, the maximum number of bytes is read,
     * or the end of serial data is reached. Will block for the first byte of input.
     * @param terminator the character to treat as the final char in the string. This char will not be in the resulting
     * string.
     * @param maxBytesToRead the maximum number of bytes to read before stopping
     * @return the complete string that was read from the port
     */
    public String readStringUntil(char terminator, int maxBytesToRead) {
        @SuppressWarnings("StringBufferMayBeStringBuilder") // A buffer makes more sense here
        StringBuffer buffer = new StringBuffer();

        int totalBytesRead = 0;
        String s;
        do {
            byte[] bytes = new byte[1];
            int result = readBytes(bytes, 1);

            s = new String(bytes, StandardCharsets.UTF_8);
            s = s.substring(0, result);

            totalBytesRead++;

            if (s.charAt(0) == terminator) {
                break;
            }

            buffer.append(s);
        } while(s.charAt(0) != terminator && totalBytesRead < maxBytesToRead);

        return buffer.toString();
    }

    /**
     * Flush all output buffers and close the serial port.
     */
    @Override
    public void close() {
        flushIOBuffers();
        closePort();
    }

    /**
     * @param o the object to compare to
     * @return true if both objects refer to the same serial port path, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (!(o instanceof AutoSerialPort that)) {
            return false;
        }

        return Objects.equals(serialPort.getSystemPortPath(), that.serialPort.getSystemPortPath());
    }

    /**
     * @return the hash of the wrapped serial port object
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(serialPort);
    }
}
