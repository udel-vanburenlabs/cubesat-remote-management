/*
 * MIT License
 *
 * Copyright (c) 2024 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.picotunnel.util;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortIOException;
import edu.udel.vanburenlabs.cubelab.picotunnel.serial.AutoSerialPort;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Path;

import static java.text.MessageFormat.format;

/**
 * Raspberry Pi Pico MCU commands wrapper class
 */
@Slf4j
public class Commands {
    /**
     * Sets up the serial port to match the MCU parameters.
     * @param p an AutoSerialPort object
     */
    public static void setupPort(AutoSerialPort p) {
        p.setComPortParameters(115200, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
        p.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
        p.openPort();
    }

    /**
     * Gets the numeric ID from the connected Pico MCU.
     * @param p an AutoSerialPort object (make sure to run {@link Commands#setupPort} first)
     * @return the Pico ID
     * @throws SerialPortIOException if the port was disconnected before the ID could be read
     */
    public static int getPicoId(AutoSerialPort p) throws SerialPortIOException {
        String[] chunks;
        do {
            if (!Files.exists(Path.of(p.getSystemPortPath()))) {
                throw new SerialPortIOException("Port already disconnected");
            }

            sendArbitraryCommand(p, "PICO_ID");
            String resp = p.readStringUntil('\n', 16);
            chunks = resp.split(" ");
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                log.error("Interrupted while getting Pico ID:", e);
            }
        } while (chunks.length != 2);
        return Integer.parseInt(chunks[1].trim());
    }

    /**
     * Attempts to set the time offset of the connected Pico MCU.
     * @param p an AutoSerialPort object
     * @return true if the MCU acknowledged the request
     */
    public static boolean setPicoTime(AutoSerialPort p) {
        long systemTimeMicroSec = System.currentTimeMillis() * 1000L;
        sendArbitraryCommand(p, "TIME_OFFSET " + systemTimeMicroSec);
        String resp = p.readStringUntil('\n', 16);
        return resp.equalsIgnoreCase("#TIME_ACK");
    }

    /**
     * Sends a #-prefixed command to the connected Pico MCU
     * @param p an AutoSerialPort object
     * @param command the command to send
     */
    public static void sendArbitraryCommand(AutoSerialPort p, String command) {
        p.writeString(format("#{0}\n", command));
    }
}
