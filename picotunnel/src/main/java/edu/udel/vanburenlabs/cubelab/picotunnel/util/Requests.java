/*
 * MIT License
 *
 * Copyright (c) 2024 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.picotunnel.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static edu.udel.vanburenlabs.cubelab.picotunnel.PicoTunnel.API_HOST;

/**
 * HTTP requests wrapper class
 */
@Slf4j
public class Requests {
    private static final HttpClient client = HttpClient.newBuilder().build();

    public static String getCommandsJson(int picoId) throws IOException, InterruptedException {
        HttpRequest commandsReq = HttpRequest
            .newBuilder(getHostURI("/api/case-params/commands/byMcu/" + picoId))
            .GET()
            .build();

        HttpResponse<String> resp = client.send(commandsReq, HttpResponse.BodyHandlers.ofString());

        if (resp.statusCode() % 500 < 100) {
            throw new IllegalStateException("Unexpected response code: " + resp.statusCode() + " " + resp.body() +
                "\nThe database is probably not started!");
        }

        log.debug("Commands json recv: {}", resp.body());

        return resp.body();
    }

    @SuppressWarnings("unchecked")
    public static List<String> getCommands(int picoId) throws IOException, InterruptedException {
        Gson gson = new Gson();
        return new ArrayList<>((List<String>) gson.fromJson(getCommandsJson(picoId), listOfType(String.class)));
    }

    public static URI getHostURI(String path) {
        if (path.trim().startsWith("/")) {
            return URI.create("http://" + API_HOST + path);
        } else {
            return URI.create("http://" + API_HOST + "/" + path);
        }
    }

    public static TypeToken<?> mapType() {
        return TypeToken.getParameterized(Map.class, String.class, Object.class);
    }

    public static TypeToken<?> listOfMapType() {
        return TypeToken.getParameterized(List.class, mapType().getType());
    }

    public static TypeToken<?> listOfType(Class<?> type) {
        return TypeToken.getParameterized(List.class, type);
    }
}
