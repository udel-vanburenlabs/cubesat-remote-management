/*
 * MIT License
 *
 * Copyright (c) 2024 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.picotunnel;

import com.fazecast.jSerialComm.SerialPort;
import edu.udel.vanburenlabs.cubelab.picotunnel.serial.AutoSerialPort;
import edu.udel.vanburenlabs.cubelab.picotunnel.util.Commands;
import edu.udel.vanburenlabs.cubelab.picotunnel.util.Requests;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class PicoTunnel {
    public static final String API_HOST = "localhost:8080";

    private static final ConcurrentLinkedDeque<String> commandQueue = new ConcurrentLinkedDeque<>();
    private static final Hashtable<Integer, AutoSerialPort> serialPorts = new Hashtable<>();

    private static ExecutorService poolExecutor;
    private static Thread hookThread;

    private static volatile boolean running = true;

    /**
     * Handler for each active microcontroller-connected serial port. Should be called for each
     * separate device.
     */
    public static void loopPicoCommTask(int picoId, AutoSerialPort port) {
        final StringBuffer sb = new StringBuffer();
        log.info("Start communication loop for pico {}", picoId);

        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 10, 10);

        poolExecutor.submit(() -> {
           while (port.isOpen() && running && Files.exists(Path.of(port.getSystemPortPath()))) {
               if (sb.indexOf("\n") >= 0) {
                   String line;
                   synchronized (sb) {
                       int newlineIdx = sb.indexOf("\n");
                       line = sb.substring(0, newlineIdx).trim();
                       sb.delete(0, newlineIdx + 1);
                   }

                   log.debug("PICO #{} -> {}", picoId, line);

                   if (!line.startsWith("#") && !line.isEmpty()) {
                       log.info("[PICO {} OUT] {}", picoId, line.trim());
                   } else {
                       ArrayList<String> lineChunks = new ArrayList<>(List.of(line.split(" ")));
                       String command = lineChunks.remove(0);
                       switch (command) {
                           case "#GET_COMMANDS" -> {
                               try {
                                   List<String> commandsList = Requests.getCommands(picoId);
                                   for (String picoCommand : commandsList) {
                                       port.writeString("#" + picoCommand + "\n");
                                   }
                               } catch (IOException | InterruptedException e) {
                                   log.error("Failed to get commands for pico {}:", picoId, e);
                               }
                           }
                           default -> {}
                       }
                   }
               }

                while (!commandQueue.isEmpty()) {
                    String command = commandQueue.pop();
                    port.writeString(command);
                }
           }

           log.info("Port {} disconnected.", port.getSystemPortPath());
           port.close();
           synchronized (serialPorts) {
               serialPorts.remove(picoId);
           }
        });

        while (port.isOpen() && running) {
            byte[] buffer = new byte[1024];
            int result = port.readBytes(buffer, 1024);
            String s = new String(buffer, StandardCharsets.UTF_8);
            s = s.substring(0, result);
            synchronized (sb) {
                sb.append(s);
            }
        }
    }

    /**
     * Method to find connected serial ports and create their respective I/O threads (if requested).
     * @param spawnCommThreads set to true if you want to automatically spawn I/O threads and listen to the port
     */
    public static void findSerialPorts(boolean spawnCommThreads) {
        for (final SerialPort p : SerialPort.getCommPorts()) {
            final AutoSerialPort autoPort = new AutoSerialPort(p);

            if (!serialPorts.containsValue(autoPort)) {
                log.info("Found serial port: {}", p.getSystemPortPath());

                p.clearRTS();
                p.setRTS();

                Commands.setupPort(autoPort);
                AtomicInteger atomicPicoId = new AtomicInteger(0);
                try {
                    atomicPicoId.set(Commands.getPicoId(autoPort));
                } catch (Exception e) {
                    log.error("Error reading Pico ID:", e);
                    continue;
                }

                synchronized (serialPorts) {
                    int picoId = atomicPicoId.get();
                    serialPorts.put(picoId, autoPort);

                    if (spawnCommThreads) {
                        // Spawn a thread for each active device
                        CompletableFuture.runAsync(() -> {
                            Commands.setPicoTime(autoPort);

                            log.info("Time set for serial port: {}", p.getSystemPortPath());
                        }, poolExecutor)
                        .thenRunAsync(() -> {
                            poolExecutor.submit(() -> loopPicoCommTask(picoId, serialPorts.get(picoId)));
                        }, poolExecutor);
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String osName = System.getProperty("os.name");
        if (osName.trim().equalsIgnoreCase("Windows")) {
            log.error("This program will not run properly on Windows. Please use a POSIX-compliant system.");
            System.exit(3);
        }

        // Set up shutdown hook
        if (hookThread == null) {
            hookThread = new Thread(PicoTunnel::shutdownHook);
            Runtime.getRuntime().addShutdownHook(hookThread);
        }

        List<String> argList = List.of(args);

        poolExecutor = Executors.newCachedThreadPool();

        findSerialPorts(false);

        // Wait for a serial device
        while (serialPorts.isEmpty() && running) {
            if (argList.contains("-w") || argList.contains("--wait")) {
                log.info("Waiting for serial device...");

                //noinspection BusyWait
                Thread.sleep(1000L);
                findSerialPorts(false);
            } else {
                log.error("No supported serial devices found. Make sure the Microcontroller board is connected.");
                System.exit(2);
            }
        }

        // Constantly poll for newly connected serial ports.
        if (argList.contains("-d") || argList.contains("--detectAfterStart")) {
            poolExecutor.submit(() -> {
               while (running) {
                   try {
                        //noinspection BusyWait
                       Thread.sleep(1000L);
                   } catch (InterruptedException e) {
                       log.debug("Serial port detection thread interrupted:", e);
                   }
                   findSerialPorts(true);
               }
            });
        }

        // Spawn a thread for each active device
        CompletableFuture<Void> timeOffsetCF = CompletableFuture.runAsync(() -> {
            for (AutoSerialPort p : serialPorts.values()) {
                Commands.setPicoTime(p);

                log.info("Time set for serial port: {}", p.getSystemPortPath());
            }
        }, poolExecutor)
            .thenRunAsync(() -> {
                for (int picoId : serialPorts.keySet()) {
                    poolExecutor.submit(() -> loopPicoCommTask(picoId, serialPorts.get(picoId)));
                }
            }, poolExecutor);
    }

    /**
     * Clean up collections and close serial connections.
     */
    public static void shutdownHook() {
        log.info("Shutting down PicoTunnel");
        running = false;
        commandQueue.clear();

        for (AutoSerialPort p : serialPorts.values()) {
            p.close();
        }

        serialPorts.clear();
    }
}
