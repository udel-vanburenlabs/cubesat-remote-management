package edu.udel.vanburenlabs.cubelab.picotunnel;

import edu.udel.vanburenlabs.cubelab.picotunnel.util.Requests;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.IOException;

@Slf4j
class RequestsTest {

    @Test
    void testGetCommandsJson() throws IOException, InterruptedException {
        log.info(Requests.getCommandsJson(0));
    }

    @Test
    void testGetCommands() throws IOException, InterruptedException {
        log.info(String.join("\n", Requests.getCommands(0)));
    }
}