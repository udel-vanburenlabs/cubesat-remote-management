﻿# DataModels
This is a simple Java 17 library that holds all of the JPA (Java Persistence API) data model classes for most of the project. These classes are used for the translation of data from the PostgreSQL database.

The `dto` package holds all the Data Transfer Objects that are used by applications to process data.

The `entity` package holds all of the JPA Entity objects that are used by the database driver to retrieve and send data to and from the database.

The `mappers` package holds interfaces that use Google MapStruct to automatically map data between Entity classes and their respective DTO classes.

-----
### Requirements:
 - Maven 3
	 - This is the only prerequisite of this project. Maven will handle downloading all of the dependencies and compilation for you when installing the library.

### Usage:
 - `mvn clean install`
 - That's it, it's really only one step.
