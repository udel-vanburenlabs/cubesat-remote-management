package edu.udel.vanburenlabs.cubelab.datamodels.dto;

import io.micronaut.serde.annotation.Serdeable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DTO for {@link edu.udel.vanburenlabs.cubelab.datamodels.entity.PicoTimeOffsetsEntity}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Serdeable
public class PicoTimeOffsetsEntityDto implements Serializable {
    Integer picoId;
    String picoStartOffset;
}