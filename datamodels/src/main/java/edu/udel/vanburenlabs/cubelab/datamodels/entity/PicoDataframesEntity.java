/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "PICO_DATAFRAMES", schema = "dataframes", catalog = "postgres")
public class PicoDataframesEntity {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "frame_id", nullable = true)
    private Integer frameId;
    @Basic
    @Column(name = "pico_id", nullable = false)
    private int picoId;

    @Column(name = "timestamp", nullable = false, length = -1)
    private LocalDateTime timestamp;
    @Basic
    @Column(name = "pico_temp", nullable = true, precision = 0)
    private Float picoTemp;
    @Basic
    @Column(name = "sens_temp0", nullable = true, precision = 0)
    private Float sensTemp0;
    @Basic
    @Column(name = "sens_temp1", nullable = true, precision = 0)
    private Float sensTemp1;
    @Basic
    @Column(name = "sens_press0", nullable = true, precision = 0)
    private Float sensPress0;
    @Basic
    @Column(name = "sens_press1", nullable = true, precision = 0)
    private Float sensPress1;
    @Basic
    @Column(name = "sens_hum0", nullable = true, precision = 0)
    private Float sensHum0;
    @Basic
    @Column(name = "sens_hum1", nullable = true, precision = 0)
    private Float sensHum1;
    @Basic
    @Column(name = "sens_accelX0", nullable = true, precision = 0)
    private Float sensAccelX0;
    @Basic
    @Column(name = "sens_accelX1", nullable = true, precision = 0)
    private Float sensAccelX1;
    @Basic
    @Column(name = "sens_accelY0", nullable = true, precision = 0)
    private Float sensAccelY0;
    @Basic
    @Column(name = "sens_accelY1", nullable = true, precision = 0)
    private Float sensAccelY1;
    @Basic
    @Column(name = "sens_accelZ0", nullable = true, precision = 0)
    private Float sensAccelZ0;
    @Basic
    @Column(name = "sens_accelZ1", nullable = true, precision = 0)
    private Float sensAccelZ1;
    @Basic
    @Column(name = "sens_gyroX0", nullable = true, precision = 0)
    private Float sensGyroX0;
    @Basic
    @Column(name = "sens_gyroX1", nullable = true, precision = 0)
    private Float sensGyroX1;
    @Basic
    @Column(name = "sens_gyroY0", nullable = true, precision = 0)
    private Float sensGyroY0;
    @Basic
    @Column(name = "sens_gyroY1", nullable = true, precision = 0)
    private Float sensGyroY1;
    @Basic
    @Column(name = "sens_gyroZ0", nullable = true, precision = 0)
    private Float sensGyroZ0;
    @Basic
    @Column(name = "sens_gyroZ1", nullable = true, precision = 0)
    private Float sensGyroZ1;
    @Basic
    @Column(name = "message_id", nullable = false)
    private int messageId;
    @Basic
    @Column(name = "rpm0")
    private Float rpm0;
    @Basic
    @Column(name = "rpm1")
    private Float rpm1;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PicoDataframesEntity that = (PicoDataframesEntity) o;
        return picoId == that.picoId && messageId == that.messageId && Objects.equals(frameId, that.frameId) &&
            Objects.equals(timestamp, that.timestamp) && Objects.equals(picoTemp, that.picoTemp) &&
            Objects.equals(sensTemp0, that.sensTemp0) && Objects.equals(sensTemp1, that.sensTemp1) &&
            Objects.equals(sensPress0, that.sensPress0) && Objects.equals(sensPress1, that.sensPress1) &&
            Objects.equals(sensHum0, that.sensHum0) && Objects.equals(sensHum1, that.sensHum1) &&
            Objects.equals(sensAccelX0, that.sensAccelX0) && Objects.equals(sensAccelX1, that.sensAccelX1) &&
            Objects.equals(sensAccelY0, that.sensAccelY0) && Objects.equals(sensAccelY1, that.sensAccelY1) &&
            Objects.equals(sensAccelZ0, that.sensAccelZ0) && Objects.equals(sensAccelZ1, that.sensAccelZ1) &&
            Objects.equals(sensGyroX0, that.sensGyroX0) && Objects.equals(sensGyroX1, that.sensGyroX1) &&
            Objects.equals(sensGyroY0, that.sensGyroY0) && Objects.equals(sensGyroY1, that.sensGyroY1) &&
            Objects.equals(sensGyroZ0, that.sensGyroZ0) && Objects.equals(sensGyroZ1, that.sensGyroZ1) &&
            Objects.equals(rpm0, that.rpm0) && Objects.equals(rpm1, that.rpm1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            frameId, picoId, timestamp, picoTemp, sensTemp0, sensTemp1, sensPress0, sensPress1, sensHum0, sensHum1,
            sensAccelX0, sensAccelX1, sensAccelY0, sensAccelY1, sensAccelZ0, sensAccelZ1, sensGyroX0, sensGyroX1,
            sensGyroY0, sensGyroY1, sensGyroZ0, sensGyroZ1, messageId, rpm0, rpm1
        );
    }
}
