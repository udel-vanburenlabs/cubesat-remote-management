/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "CASE_FOOTAGE", schema = "cubelab_cases", catalog = "postgres")
@IdClass(CaseFootageEntityPK.class)
public class CaseFootageEntity {
    @Id
    @Column(name = "file_id", nullable = false)
    private int fileId;
    @Id
    private int caseId;
    @Id
    @Column(name = "sequence_id", nullable = false)
    private int sequenceId;
    @Basic
    @Column(name = "capture_timestamp", nullable = false, length = -1)
    private LocalDateTime captureTimestamp;
    @Basic
    @Column(name = "file_path", nullable = false, length = -1)
    private String filePath;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(
        name = "case_id", referencedColumnName = "case_id", insertable = false, updatable = false, nullable = false
    )
    @MapsId("caseId")
    private CaseParamsEntity caseField;

    @Override
    public int hashCode() {
        return Objects.hash(fileId, caseId, sequenceId, captureTimestamp, filePath);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CaseFootageEntity that = (CaseFootageEntity) o;
        return caseId == that.caseId && sequenceId == that.sequenceId && Objects.equals(fileId, that.fileId) &&
            Objects.equals(captureTimestamp, that.captureTimestamp) && Objects.equals(filePath, that.filePath);
    }
}
