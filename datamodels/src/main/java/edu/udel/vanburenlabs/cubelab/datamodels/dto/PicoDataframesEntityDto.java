/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.dto;

import io.micronaut.serde.annotation.Serdeable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DTO for {@link edu.udel.vanburenlabs.cubelab.datamodels.entity.PicoDataframesEntity}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Serdeable
public class PicoDataframesEntityDto implements Serializable {
    Integer frameId;
    int picoId;
    LocalDateTime timestamp;
    Float picoTemp;
    Float sensTemp0;
    Float sensTemp1;
    Float sensPress0;
    Float sensPress1;
    Float sensHum0;
    Float sensHum1;
    Float sensAccelX0;
    Float sensAccelX1;
    Float sensAccelY0;
    Float sensAccelY1;
    Float sensAccelZ0;
    Float sensAccelZ1;
    Float sensGyroX0;
    Float sensGyroX1;
    Float sensGyroY0;
    Float sensGyroY1;
    Float sensGyroZ0;
    Float sensGyroZ1;
    Float rpm0;
    Float rpm1;

    int messageId;
}