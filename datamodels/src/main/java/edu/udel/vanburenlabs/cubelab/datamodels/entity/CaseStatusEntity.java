/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "CASE_STATUS", schema = "cubelab_cases", catalog = "postgres")
public class CaseStatusEntity {
    @Id
    @Column(name = "case_id", nullable = true)
    private Integer caseId;
    @Basic
    @Column(name = "case_status_id", nullable = false)
    private int caseStatusId;

    @Column(name = "start_timestamp", nullable = true, length = -1)
    private LocalDateTime startTimestamp;

    @Column(name = "complete_timestamp", nullable = true, length = -1)
    private LocalDateTime completeTimestamp;
    @Basic
    @Column(name = "average_fps", nullable = true, precision = 0)
    private Float averageFps;
    @Basic
    @Column(name = "frames_captured", nullable = true)
    private Integer framesCaptured;

    @Override
    public int hashCode() {
        return Objects.hash(caseId, caseStatusId, startTimestamp, completeTimestamp, averageFps, framesCaptured);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CaseStatusEntity that = (CaseStatusEntity) o;
        return caseStatusId == that.caseStatusId && Objects.equals(caseId, that.caseId) &&
            Objects.equals(startTimestamp, that.startTimestamp) &&
            Objects.equals(completeTimestamp, that.completeTimestamp) && Objects.equals(averageFps, that.averageFps) &&
            Objects.equals(framesCaptured, that.framesCaptured);
    }
}
