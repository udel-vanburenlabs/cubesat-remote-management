/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "CASE_PARAMS", schema = "cubelab_cases", catalog = "postgres")
public class CaseParamsEntity {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "case_id", nullable = true)
    private Integer caseId;
    @Basic
    @Column(name = "flow_facility", nullable = false)
    private int flowFacility;
    @Basic
    @Column(name = "capture_duration_sec", nullable = false)
    private int captureDurationSec;
    @Basic
    @Column(name = "stepper_speed", nullable = false)
    private int stepperSpeed;
    @Basic
    @Column(name = "case_type_id", nullable = false)
    private int caseTypeId;
    @Basic
    @Column(name = "spin_duration_sec", nullable = false)
    private int spinDurationSec;
    @Basic
    @Column(name = "laser_brightness", nullable = false)
    private int laserBrightness;
    @Basic
    @Column(name = "min_sec_since_last", nullable = false)
    private int minSecSinceLast;
    @Basic
    @Column(name = "max_temp", nullable = true, precision = 0)
    private Float maxTemp;
    @Basic
    @Column(name = "min_temp", nullable = true, precision = 0)
    private Float minTemp;
    @Basic
    @Column(name = "capture_type_id", nullable = false)
    private int captureTypeId;
    @Basic
    @Column(name = "burst_period_ms", nullable = true)
    private Integer burstPeriodMs;
    @Basic
    @Column(name = "burst_size", nullable = true)
    private Integer burstSize;
    @Basic
    @Column(name = "target_fps", nullable = true)
    private Integer targetFps;
    @Basic
    @Column(name = "backup_priority", nullable = false)
    private Integer backupPriority;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaseParamsEntity that)) {
            return false;
        }
        return getFlowFacility() == that.getFlowFacility() && getCaptureDurationSec() == that.getCaptureDurationSec() &&
            getStepperSpeed() == that.getStepperSpeed() && getCaseTypeId() == that.getCaseTypeId() &&
            getSpinDurationSec() == that.getSpinDurationSec() && getLaserBrightness() == that.getLaserBrightness() &&
            getMinSecSinceLast() == that.getMinSecSinceLast() && getCaptureTypeId() == that.getCaptureTypeId() &&
            Objects.equals(getCaseId(), that.getCaseId()) &&
            Objects.equals(getMaxTemp(), that.getMaxTemp()) &&
            Objects.equals(getMinTemp(), that.getMinTemp()) &&
            Objects.equals(getBurstPeriodMs(), that.getBurstPeriodMs()) &&
            Objects.equals(getBurstSize(), that.getBurstSize()) &&
            Objects.equals(getTargetFps(), that.getTargetFps()) &&
            Objects.equals(getBackupPriority(), that.getBackupPriority());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCaseId(), getFlowFacility(), getCaptureDurationSec(), getStepperSpeed(), getCaseTypeId(),
            getSpinDurationSec(), getLaserBrightness(), getMinSecSinceLast(), getMaxTemp(), getMinTemp(),
            getCaptureTypeId(), getBurstPeriodMs(), getBurstSize(), getTargetFps(), getBackupPriority()
        );
    }
}
