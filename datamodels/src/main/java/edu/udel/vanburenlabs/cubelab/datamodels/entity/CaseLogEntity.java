/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.datamodels.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "CASE_LOG", schema = "cubelab_cases", catalog = "postgres")
@IdClass(CaseLogEntityPK.class)
public class CaseLogEntity {
    @Id
    @Column(name = "case_id", nullable = false)
    private int caseId;
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "event_id", nullable = false)
    private int eventId;

    @Column(name = "timestamp", nullable = false, length = -1)
    private LocalDateTime timestamp;

    @Basic
    @Column(name = "log_level", nullable = false)
    private int logLevel;

    @Basic
    @Column(name = "log_message", nullable = true, length = -1)
    private String logMessage;

    @Override
    public int hashCode() {
        return Objects.hash(caseId, eventId, timestamp, logMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CaseLogEntity that = (CaseLogEntity) o;
        return caseId == that.caseId && eventId == that.eventId && Objects.equals(timestamp, that.timestamp) &&
            Objects.equals(logMessage, that.logMessage);
    }
}
