# Cubelab Case Coordinator App

### Authors:

Galen Nare

---

A Java application to handle coordination of experimental cases, and the capture of video
data from the onboard FLIR cameras. This app is designed to run on the Orange Pi 5 SBC;
however, it can be tested on a normal Linux workstation. Please see the top-level comment
on the `CaseCoordinator` class for more information.

The CubeLab Camera Capture project must already be installed for this project to work. Please see
its repo here for more information: 
https://gitlab.eecis.udel.edu/udel-senior-design-2022-team-133/cubelab-camera-frame-straddle

### Services:

This app has 2 Systemd services that can be installed to allow it to run in the background, and automatically
restart on failure.

 - `case-coordinator.service` - The main service that runs the app. This should be installed and enabled
in the production build.
 - `cubelab-stress-test.service` - This service can be used to constantly run the cameras in a loop, without
affecting case parameters or status. This should be used to test the CubeLab's heat dissipation ability
and power consumption, and can be installed for the NanoRacks tests.

### Top-level dependencies:

- _rest-api_ - Depends on the REST API server being installed for its transitive
dependencies, as well as some utility helper classes that are defined in the project.
Please make sure this project is installed in the local repo before running this one.
- _datamodels_ - Used for DTO objects to properly interact with the REST API.
- _Gson_ - Used by the HTTP client class to encode/decode JSON data.
- _SnakeYAML_ - Used to decode the config.
- _SLF4J and Logback_ - Logging interface to enable pretty-printed messages and various log levels.

### How to run/install:

- Clone the repo using `git clone`
- Ensure Maven is installed (version 3.8 is preferred).
- Ensure the Datamodels project has already been installed (../datamodels).
- Ensure the REST-API project has already been installed (../rest-api).
- Ensure the Camera capture scripts are compiled and installed in their correct
locations. Additionally, make sure that the `config.yml` file in this project
correctly points to these scripts, and all the other config values are correct.
- Make sure the REST API server is running on port `8080`.
- To run the app, run `mvn clean compile exec:java`.
- To install to the local repo and build a JAR, run `mvn clean install`

### Connection Info:

- Make sure the `rest-api` is running on port `8080`.

### License:

MIT

Feel free to use this code for whatever you want (credit appreciated but not required),
but if you claim you wrote it I will publicly shame you :)