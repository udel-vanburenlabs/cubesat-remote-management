/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord;

import com.github.joselion.maybe.Maybe;
import edu.udel.vanburenlabs.cubelab.casecoord.config.CoordConfigurator;
import edu.udel.vanburenlabs.cubelab.casecoord.exceptions.InvalidDataException;
import edu.udel.vanburenlabs.cubelab.casecoord.http.Requests;
import edu.udel.vanburenlabs.cubelab.casecoord.scripts.TcpTunWrapper;
import edu.udel.vanburenlabs.cubelab.casecoord.util.Pair;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaptureTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusesEntityDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.http.HttpClient;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Main class for the Cubelab case coordinator.
 * <p /><p />
 * <b>IMPORTANT!!!</b> REQUIRES the <code>rest-api</code> project to be running and <code>config.yml</code> to be
 * properly configured.
 * <p /><p />
 * The program uses a singleton instance of this class to manage all global state,
 * and interacts with the rest-api service to provide all data for cases. This uses the {@link java.net.http} package
 * classes for these requests. This program produces multiple threads to handle cases and the concurrent scripts used to
 * execute them (some of these scripts are wrapped in classes used here, see:
 * {@link edu.udel.vanburenlabs.cubelab.casecoord.scripts}).
 * <p /><p />
 * A fail-fast design was used for this program;
 * any failures that prevent the coordinator from completing any cases will cause its termination. If a failure is
 * isolated to a case, it will instead fail the case and attempt to properly tag it, and move on to the next case.
 * <p/><p />
 * This is not designed to self-terminate under normal circumstances. The program will continue to poll the API
 * for new cases (every second) until one is available, or until the user interrupts the program. The program will then
 * inform the API and gracefully shut down. In the event of failures, the program will still attempt to shut down
 * gracefully (unless the JVM is forcibly terminated).
 */
@Slf4j
@Getter
public class CaseCoordinator {
    @Getter
    private static CaseCoordinator instance;

    private static TcpTunWrapper tcpTunWrapper;
    private static final ArrayList<CaseParamsEntityDto> cachedCases = new ArrayList<>();
    private static final ExecutorService execService = Executors.newCachedThreadPool();

    private static final AtomicInteger shutdownLevel = new AtomicInteger(0);

    private Map<String, Object> config;

    private final CaseExecutor executor;

    private final HttpClient client = HttpClient.newBuilder().build();

    private final Hashtable<Integer, CaptureTypesEntityDto> captureTypes = new Hashtable<>();
    private final Hashtable<Integer, CaseStatusesEntityDto> statusTypes = new Hashtable<>();

    private Maybe<Pair<CaseParamsEntityDto, CaseStatusEntityDto>> runningCase = Maybe.nothing();

    private LocalDateTime lastCaseCompletedTime = LocalDateTime.now();

    private final String apiHost;

    @Getter(AccessLevel.NONE)
    private final CountDownLatch pollLock = new CountDownLatch(1);

    @Getter(AccessLevel.NONE)
    private LocalDateTime casesLastRefreshed = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);

    @Getter(AccessLevel.NONE)
    private Thread tcpTunWatchdogThread;

    @Getter(AccessLevel.NONE)
    private Timer casePollTimer;

    @Getter(AccessLevel.NONE)
    private boolean stopAfterNextCase = false;

    @Getter(AccessLevel.NONE)
    private TimerTask casePollTask;

    @Getter(AccessLevel.NONE)
    private Future<?> caseLogicFuture;

    private static boolean stressTest = false;

    /**
     * Sets up, but does not start the coordinator singleton object. Sets the singleton <code>instance</code>
     * to the current object. Configures a shutdown hook for the program.
     */
    public CaseCoordinator() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        SignalHandler termHandler = signal -> shutdownLevel.set(2);
        Signal.handle(new Signal("TERM"), termHandler);
        SignalHandler intHandler = signal -> shutdownLevel.set(1);
        Signal.handle(new Signal("INT"), intHandler);

        if (instance == null) {
            instance = this;
        }

        configCoordinator();

        tcpTunWrapper = new TcpTunWrapper(String.valueOf(config.get("pico-tcp-tunnel-path")));

        apiHost = String.valueOf(config.get("api-host"));

        postCoordinatorMessage("coordinator_start");
        getStaticDbMappings();

        executor = new CaseExecutor(this);
    }

    /**
     * Main method, instantiates and starts the coordinator object.
     *
     * @param args - command line arguments (currently unused)
     */
    public static void main(String[] args) {
        List<String> argsList = Arrays.asList(args);

        stressTest = (argsList.contains("--stress-test") || argsList.contains("-S"));
        if (stressTest) {
            log.warn("Stress test mode selected. The CubeLab will now run with all modules powered until this program" +
                "is interrupted.");
        }

        log.info("Starting CubeLab case coordinator");

        CaseCoordinator coordinator = new CaseCoordinator();

        coordinator.tcpTunWatchdogThread = coordinator.runPicoTcpTunnel();

        coordinator.runCasePollTimer();

        log.info("Exiting.");
    }

    /**
     * Creates and runs the case poll timer.
     */
    private void runCasePollTimer() {
        log.info("Starting case poll timer after 3s...");
        casePollTimer = createPollTimer();

        try {
            pollLock.await();
        } catch (InterruptedException e) {
            log.error("Main thread interrupted while waiting: ", e);
        }

        casePollTimer.cancel();
    }

    /**
     * Creates a {@link Timer} object that periodically polls for queued cases from the API. This runs the queued cases
     * and processes the data from the data capture scripts. Any failures are logged to the database.
     * Queued cases are cached in memory, and re-checked every two minutes, or when the cache is emptied.
     * <p/>
     * Does not start the timer.
     *
     * @return - the Timer object
     */
    private Timer createPollTimer() {
        Timer casePollTimer = new Timer("casePollTimer");
        casePollTask = new TimerTask() {
            @Override
            public void run() {
                caseLogicFuture = execService.submit(() -> {
                    if (stressTest) {
                        try {
                            executor.getContWrapper().capture(
                                0,
                                175 * 30,
                                String.valueOf(getConfig().get("target-footage-path")));
                        } catch (IOException | InterruptedException | ExecutionException e) {
                            log.error("Error while capturing video for stress test: ", e);
                        }
                    }

                    if (!pollCases()) {
                        return;
                    }

                    if (!cachedCases.isEmpty()) {
                        CaseParamsEntityDto nextCase = cachedCases.get(0);
                        Optional<CaseStatusEntityDto> caseStatusDto = getCaseStatusDto(nextCase);

                        if (caseStatusDto.isEmpty()) {
                            return;
                        }

                        if (!checkRunningCases(nextCase, caseStatusDto.orElseThrow())) {
                            return;
                        }

                        log.debug("Popping case from the cache.");
                        cachedCases.remove(0);

                        sleepUntilNextCase(nextCase);

                        if (!postStartMessage(nextCase)) {
                            return;
                        }

                        if (!postCaseStartTimestamp(nextCase)) {
                            return;
                        }

                        if (!postInProgressStatus(nextCase)) {
                            return;
                        }

                        boolean success = false;
                        try {
                            success = executor.run(nextCase);
                        } catch (InvalidDataException e) {
                            log.error("Invalid data from server: ", e);
                            System.exit(-100);
                        }

                        if (success) {
                            finalizeCase(nextCase);
                            // TODO post to case footage table
                            postCompleteMessage(nextCase);
                        }
                    }
                });

                try {
                    caseLogicFuture.get();
                } catch (InterruptedException | ExecutionException e) {
                    log.error("Case interrupted: ", e);
                }

                runningCase = Maybe.nothing();
                lastCaseCompletedTime = LocalDateTime.now();

                if (stopAfterNextCase) {
                    casePollTimer.cancel();
                }
            }
        };

        casePollTimer.schedule(casePollTask, 3000L, 1000L);
        return casePollTimer;
    }

    private boolean postCaseStartTimestamp(CaseParamsEntityDto nextCase) {
        try {
            Requests.setCaseStartTimestamp(client, nextCase.getCaseId(), LocalDateTime.now());
        } catch (IOException | InterruptedException e) {
            log.error("Failed to set start timestamp for case {}: ", nextCase.getCaseId(), e);
            executor.failCase(nextCase);
            return false;
        }
        return true;
    }

    private boolean postCompleteMessage(CaseParamsEntityDto nextCase) {
        try {
            Requests.postHubAuditMessage(client, "completed_case");
        } catch (IOException | InterruptedException e) {
            log.error("Failed to post completed message for case {}: ", nextCase.getCaseId(), e);
            return false;
        }
        return true;
    }

    private void sleepUntilNextCase(@NotNull CaseParamsEntityDto nextCase) {
        long secSinceLastCase = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) -
            lastCaseCompletedTime.toEpochSecond(ZoneOffset.UTC);

        if (secSinceLastCase <= nextCase.getMinSecSinceLast()) {
            try {
                Requests.setCaseStatus(client, nextCase.getCaseId(), "sleeping");
                long secToWait = nextCase.getMinSecSinceLast() - secSinceLastCase;
                log.info("Waiting {} seconds to start case {}.", secToWait, nextCase.getCaseId());
                Thread.sleep(secToWait * 1000);
            } catch (InterruptedException | IOException e) {
                log.error("Exception while sleeping before new case: ", e);
                sleepUntilNextCase(nextCase);
            }
        }
    }

    private void postCoordinatorMessage(@NotNull String message) {
        try {
            Requests.postHubAuditMessage(client, message);
        } catch (IOException | InterruptedException e) {
            log.warn("API server may not be running or not properly configured. Check your running services!");
            log.error("Exception caught posting audit message: ", e);
        }
    }

    private void getStaticDbMappings() {
        try {
            Requests.getCaptureTypes(client)
                .forEach(e -> captureTypes.put(e.getCaptureTypeId(), e));
        } catch (IOException | InterruptedException e) {
            log.error("Failed to get capture types: ", e);
            System.exit(-3);
        }

        try {
            Requests.getStatusTypes(client)
                .forEach(e -> statusTypes.put(e.getCaseStatusId(), e));
        } catch (IOException | InterruptedException e) {
            log.error("Failed to get status types: ", e);
            System.exit(-3);
        }
    }

    private void configCoordinator() {
        try {
            config = CoordConfigurator.getConfig();
        } catch (IOException e) {
            log.error("Failed to load config.yml!", e);
            System.exit(-2);
        }
    }

    private Thread runPicoTcpTunnel() {
        Thread tcpTunWatchdogThread = new Thread(() -> {
            while (true) {
                try {
                    log.info("Starting Pico TCP tunnel.");
                    tcpTunWrapper.run();
                } catch (IOException | ExecutionException e) {
                    log.error("Failed to start TCP tunnel: ", e);
                    System.exit(-4);
                } catch (InterruptedException e) {
                    log.info("TCP tunnel interrupted:");
                    log.debug("", e);
                }
            }
        }, "PicoTcpTunThread");

        tcpTunWatchdogThread.start();
        return tcpTunWatchdogThread;
    }

    private boolean pollCases() {
        try {
            if (cachedCases.isEmpty() || LocalDateTime.now().isAfter(casesLastRefreshed.plusMinutes(2))) {
                log.debug("Polling queued cases");
                cachedCases.clear();
                cachedCases.addAll(Requests.getQueuedCases(client));
                log.debug("Queued case parameters: " + cachedCases);
                casesLastRefreshed = LocalDateTime.now();
            }
        } catch (IOException | InterruptedException e) {
            log.error("Failed to request queued cases: ", e);
            return false;
        }
        return true;
    }

    private Optional<CaseStatusEntityDto> getCaseStatusDto(@NotNull CaseParamsEntityDto nextCase) {
        try {
            return Optional.of(Requests.getCaseStatus(client, nextCase.getCaseId()));
        } catch (IOException | InterruptedException e) {
            log.error("Failed to get case status object for id {}: ", nextCase.getCaseId(), e);
            executor.failCase(nextCase);
            return Optional.empty();
        }
    }

    private boolean checkRunningCases(CaseParamsEntityDto nextCase, CaseStatusEntityDto caseStatusDto) {
        if (runningCase.hasNothing()) {
            runningCase = Maybe.just(new Pair<>(nextCase, caseStatusDto));
        } else {
            log.debug("Cubelab is busy with another case, waiting...");
            return false;
        }

        return true;
    }

    private void finalizeCase(@NotNull CaseParamsEntityDto nextCase) {
        try {
            Requests.setCaseStatus(client, nextCase.getCaseId(), "complete");
            Requests.setCaseCompleteTimestamp(client, nextCase.getCaseId(), LocalDateTime.now());
        } catch (IOException | InterruptedException e) {
            log.error("Failed to complete case {}: ", nextCase.getCaseId(), e);
            executor.failCase(nextCase);
        }
    }

    private boolean postInProgressStatus(@NotNull CaseParamsEntityDto nextCase) {
        try {
            Requests.setCaseStatus(client, nextCase.getCaseId(), "in_progress");
        } catch (IOException | InterruptedException e) {
            log.error("Failed to start case {}: ", nextCase.getCaseId(), e);
            return false;
        }
        return true;
    }

    private boolean postStartMessage(@NotNull CaseParamsEntityDto nextCase) {
        log.info("Starting queued case {}", nextCase.getCaseId());
        try {
            Requests.postHubAuditMessage(client, "start_case");
        } catch (IOException | InterruptedException e) {
            log.error("Exception caught posting audit message: ", e);
            return false;
        }
        return true;
    }

    /**
     * Utility method to get the current running case objects, if any.
     *
     * @return - An optional containing a pair of the running {@link CaseParamsEntityDto} and
     * {@link CaseStatusEntityDto} objects.
     */
    public Optional<Pair<CaseParamsEntityDto, CaseStatusEntityDto>> getRunningCase() {
        return runningCase.toOptional();
    }

    /**
     * Gracefully shuts down the coordinator.
     */
    public void shutdown() {
        shutdownLevel.incrementAndGet();

        if (shutdownLevel.get() < 2 && !stressTest) {
            log.info("Signal recieved. Stopping after next case. Send again for immediate shut down.");
            stopAfterNextCase = true;
            while (!caseLogicFuture.isDone() && shutdownLevel.get() < 2) {
                try {
                    //noinspection BusyWait
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                    log.error("Interrupted shutdown wait: ", e);
                }
            }
        }

        log.info("Shutting down.");

        if (casePollTimer != null) {
            casePollTimer.cancel();
            casePollTask.cancel();
        }

        if (caseLogicFuture != null) {
            caseLogicFuture.cancel(true);
            execService.shutdownNow();
        }

        if (tcpTunWatchdogThread != null) {
            tcpTunWatchdogThread.interrupt();
        }

        postCoordinatorMessage("coordinator_stop");
    }
}
