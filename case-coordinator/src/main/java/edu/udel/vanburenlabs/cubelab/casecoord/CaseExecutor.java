/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord;

import edu.udel.vanburenlabs.cubelab.casecoord.exceptions.InvalidDataException;
import edu.udel.vanburenlabs.cubelab.casecoord.http.Requests;
import edu.udel.vanburenlabs.cubelab.casecoord.scripts.BurstCaptureWrapper;
import edu.udel.vanburenlabs.cubelab.casecoord.scripts.ContinuousCaptureWrapper;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaptureTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseFootageEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusesEntityDto;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Slf4j
@Getter
public class CaseExecutor {
    private final CaseCoordinator coordinator;
    private final ContinuousCaptureWrapper contWrapper;
    private final BurstCaptureWrapper burstWrapper;

    public CaseExecutor(@NotNull CaseCoordinator coordinator) {
        this.coordinator = coordinator;

        contWrapper =
            new ContinuousCaptureWrapper(String.valueOf(coordinator.getConfig().get("cont-capture-path")));

        burstWrapper =
            new BurstCaptureWrapper(String.valueOf(coordinator.getConfig().get("burst-capture-path")));
    }

    public boolean run(@NotNull CaseParamsEntityDto caseParams) throws InvalidDataException {
        CaptureTypesEntityDto captureType = coordinator.getCaptureTypes().get(caseParams.getCaptureTypeId());

        if (captureType == null) {
            failCase(caseParams);
            throw new InvalidDataException("Invalid capture type");
        }

        try {
            Requests.postHubAuditMessage(coordinator.getClient(), "start_capture");
        } catch (IOException | InterruptedException e) {
            log.error("Exception caught posting audit message: ", e);
            return false;
        }

        Optional<String> footagePath;
        if (captureType.getCaptureType().equalsIgnoreCase("cont")) {
            int frames = caseParams.getCaptureDurationSec() * caseParams.getTargetFps();
            try {
                footagePath = contWrapper.capture(
                    caseParams.getFlowFacility(),
                    frames,
                    String.valueOf(coordinator.getConfig().get("target-footage-path"))
                );
            } catch (IOException | InterruptedException | ExecutionException e) {
                log.error("Failed to capture for case {}: ", caseParams.getCaseId(), e);
                failCase(caseParams);
                return false;
            }
        } else if (captureType.getCaptureType().equalsIgnoreCase("burst")) {
            int bursts = caseParams.getCaptureDurationSec() / caseParams.getBurstPeriodMs();
            try {
                footagePath = burstWrapper.capture(
                    caseParams.getFlowFacility(),
                    (double) caseParams.getBurstPeriodMs() / 1000,
                    bursts,
                    caseParams.getBurstSize(),
                    String.valueOf(coordinator.getConfig().get("target-footage-path"))
                );
            } catch (IOException | InterruptedException | ExecutionException e) {
                log.error("Failed to capture burst for case {}: ", caseParams.getCaseId(), e);
                failCase(caseParams);
                return false;
            }
        } else {
            failCase(caseParams);
            throw new InvalidDataException("Invalid capture type");
        }

        try {
            Requests.postHubAuditMessage(coordinator.getClient(), "end_capture");
        } catch (IOException | InterruptedException e) {
            log.error("Exception caught posting audit message: ", e);
            return false;
        }

        if (footagePath.isEmpty()) {
            log.warn("Failed to get footage path for case {}", caseParams.getCaseId());
            failCase(caseParams);
            return false;
        }

        CaseFootageEntityDto dto = new CaseFootageEntityDto(
            1,
            caseParams.getCaseId(),
            caseParams.getCaseId(),
            LocalDateTime.now(),
            footagePath.get());

        try {
            Requests.postFootage(coordinator.getClient(), dto);
        } catch (IOException | InterruptedException e) {
            log.warn("Failed write footage to API for case {}", caseParams.getCaseId(), e);
        }

        return true;
    }

    public void failCase(@NotNull CaseParamsEntityDto caseParams) {
        log.warn("Case id {} failed", caseParams.getCaseId());
        CaseStatusesEntityDto failed = coordinator.getStatusTypes()
            .get(
                coordinator
                    .getStatusTypes()
                    .values()
                    .stream()
                    .filter(e -> e.getCaseStatus().equalsIgnoreCase("failed"))
                    .findFirst()
                    .orElseThrow()
                    .getCaseStatusId()
            );

        try {
            Requests.setCaseStatus(coordinator.getClient(), caseParams.getCaseId(), failed.getCaseStatusId());
            Requests.postHubAuditMessage(coordinator.getClient(), "fail_case");
        } catch (IOException | InterruptedException ex) {
            log.error("Failed to set case status for {}: ", caseParams.getCaseId(), ex);
        }
    }
}
