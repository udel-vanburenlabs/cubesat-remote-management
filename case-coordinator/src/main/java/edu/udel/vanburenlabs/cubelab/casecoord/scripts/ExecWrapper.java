/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord.scripts;

import edu.udel.vanburenlabs.cubelab.casecoord.CaseCoordinator;
import edu.udel.vanburenlabs.cubelab.casecoord.http.Requests;
import edu.udel.vanburenlabs.cubelab.casecoord.util.Pair;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
public abstract class ExecWrapper {
    protected final String execPath;

    protected Pair<Timer, CompletableFuture<Pair<StringBuffer, StringBuffer>>> handleBuffers(Process proc, String timerName, boolean output) {
        InputStream stdoutStream = proc.getInputStream();
        InputStream stderrStream = proc.getErrorStream();

        StringBuffer stdout = new StringBuffer();
        StringBuffer stderr = new StringBuffer();

        BufferedReader outReader = new BufferedReader(new InputStreamReader(stdoutStream));
        BufferedReader errReader = new BufferedReader(new InputStreamReader(stderrStream));

        Timer outTimer = new Timer(timerName);
        outTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    while (outReader.ready()) {
                        String s = outReader.readLine();
                        stdout.append(s).append("\n");
                        if (output) {
                            log.debug(s);
                        }
                    }

                    while (errReader.ready()) {
                        String s = errReader.readLine();
                        stderr.append(s).append("\n");
                        if (output) {
                            log.debug(s);
                        }
                    }
                } catch (IOException e) {
                    log.error("Failed to read from stdout/stderr stream: ", e);
                }
            }
        }, 100L, 100L);

        CompletableFuture<Pair<StringBuffer, StringBuffer>> future = proc.onExit().thenApply(process -> {
            outTimer.cancel();

            try {
                while (outReader.ready()) {
                    String s = outReader.readLine();
                    stdout.append(s).append("\n");
                    log.debug(s);
                }

                while (errReader.ready()) {
                    String s = errReader.readLine();
                    stderr.append(s).append("\n");
                    log.debug(s);
                }
            } catch (IOException e) {
                log.error("Failed to read from stdout/stderr stream: ", e);
            }

            return new Pair<>(stdout, stderr);
        });

        return new Pair<>(outTimer, future);
    }

    protected Pair<Timer, CompletableFuture<Pair<StringBuffer, StringBuffer>>> handleBuffers(Process proc, String timerName) {
        return handleBuffers(proc, timerName, false);
    }

    protected void captureFailMethod(Process proc, StringBuffer stderr) {
        if (proc.exitValue() == 137) {
            log.error("Capture returned 137, likely attempted to allocate too much memory!");
            try {
                Requests.postHubAuditMessage(CaseCoordinator.getInstance().getClient(), "out_of_memory");
            } catch (IOException | InterruptedException e) {
                log.error("Exception caught posting audit message: ", e);
            }
        } else if (proc.exitValue() == 255) {
            log.error("Capture returned 255, could not access camera");
            try {
                Requests.postHubAuditMessage(CaseCoordinator.getInstance().getClient(), "err_fail_camera");
            } catch (IOException | InterruptedException e) {
                log.error("Exception caught posting audit message: ", e);
            }
        } else if (proc.exitValue() == 254) {
            log.error("Capture returned 254, camera not connected");
            try {
                Requests.postHubAuditMessage(CaseCoordinator.getInstance().getClient(), "err_no_camera");
            } catch (IOException | InterruptedException e) {
                log.error("Exception caught posting audit message: ", e);
            }
        }
        log.error("Capture returned non-zero exit status");
        log.error(stderr.toString());

        try {
            Requests.postHubAuditMessage(CaseCoordinator.getInstance().getClient(), "capture_fail");
        } catch (IOException | InterruptedException e) {
            log.error("Exception caught posting audit message: ", e);
        }
    }
}
