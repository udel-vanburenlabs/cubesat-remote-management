/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord.scripts;

import edu.udel.vanburenlabs.cubelab.casecoord.util.Pair;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Timer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.lang.ProcessBuilder.Redirect.PIPE;

@Slf4j
public class TcpTunWrapper extends ExecWrapper {
    public TcpTunWrapper(String execPath) {
        super(execPath);
    }

    public void run() throws IOException, ExecutionException, InterruptedException {
        run("localhost", 8080);
    }

    public void run(String host, int port) throws IOException, ExecutionException, InterruptedException {
        Process proc = new ProcessBuilder(
            execPath,
            host,
            String.valueOf(port)
        )
            .redirectOutput(PIPE)
            .redirectError(PIPE)
            .redirectInput(PIPE)
            .start();

        long pid = proc.pid();
        log.debug("Waiting for pid {}", pid);

        Pair<Timer, CompletableFuture<Pair<StringBuffer, StringBuffer>>> out =
            handleBuffers(proc, "burstCaptureTimer");

        CompletableFuture<Pair<StringBuffer, StringBuffer>> future = out.second();

        Pair<StringBuffer, StringBuffer> outAndErr = future.get();

        StringBuffer stdout = outAndErr.first();
        StringBuffer stderr = outAndErr.second();

        log.info("Pico TCPTun exited.");
        log.debug(stdout.toString());
        if (proc.exitValue() != 0) {
            log.error("Pico TCPTun returned non-zero exit status");
            log.error(stderr.toString());
        }
    }
}
