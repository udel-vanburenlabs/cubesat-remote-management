/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord.http;

import com.google.gson.reflect.TypeToken;
import edu.udel.vanburenlabs.cubelab.casecoord.CaseCoordinator;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaptureTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseFootageEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabAuditEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabHubEventTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabPicoEventTypesEntityDto;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;

@Slf4j
public final class Requests {

    public static HttpResponse<String> postHubAuditMessage(@NotNull HttpClient hc, String type)
        throws IOException, InterruptedException {
        HttpRequest hubTypesReq = HttpRequest
            .newBuilder(getHostURI("/api/audit/hubTypes/name/" + type.toLowerCase()))
            .GET()
            .build();
        HttpRequest hubTypePost = HttpRequest
            .newBuilder(getHostURI("/api/audit/hubTypes"))
            .POST(
                HttpRequest.BodyPublishers.ofString(
                    toJsonString(new CubelabHubEventTypesEntityDto(0, type.toLowerCase()))
                )
            )
            .build();

        int id;
        HttpResponse<String> hubTypesResp = hc.send(hubTypesReq, HttpResponse.BodyHandlers.ofString());
        CubelabHubEventTypesEntityDto hubType =
            fromJsonString(hubTypesResp.body(), CubelabHubEventTypesEntityDto.class);
        if (hubType == null || hubType.getHubEventTypeId() == null) {
            HttpResponse<String> hubTypePostResp = hc.send(hubTypePost, HttpResponse.BodyHandlers.ofString());
            String[] locParts = hubTypePostResp.headers().firstValue("location").orElseThrow().split("/");
            if (locParts.length < 1) {
                throw new IOException("Missing hub type location URI");
            }
            id = Integer.parseInt(locParts[locParts.length - 1]);
        } else {
            id = hubType.getHubEventTypeId();
        }

        CubelabAuditEntityDto dto = new CubelabAuditEntityDto(0, LocalDateTime.now().toString(), id, 1);

        HttpRequest msqReq = HttpRequest
            .newBuilder(getHostURI("/api/audit"))
            .POST(HttpRequest.BodyPublishers.ofString(toJsonString(dto)))
            .build();

        return hc.send(msqReq, HttpResponse.BodyHandlers.ofString());
    }

    public static HttpResponse<String> postPicoAuditMessage(@NotNull HttpClient hc, String type)
        throws IOException, InterruptedException {
        HttpRequest picoTypesReq = HttpRequest
            .newBuilder(getHostURI("/api/audit/picoTypes/name/" + type.toLowerCase()))
            .GET()
            .build();
        HttpRequest picoTypePost = HttpRequest
            .newBuilder(getHostURI("/api/audit/picoTypes"))
            .POST(
                HttpRequest.BodyPublishers.ofString(
                    toJsonString(new CubelabPicoEventTypesEntityDto(0, type.toLowerCase()))
                )
            )
            .build();

        int id;
        HttpResponse<String> picoTypesResp = hc.send(picoTypesReq, HttpResponse.BodyHandlers.ofString());
        CubelabPicoEventTypesEntityDto picoType =
            fromJsonString(picoTypesResp.body(), CubelabPicoEventTypesEntityDto.class);
        if (picoType == null || picoType.getPicoEventTypeId() == null) {
            HttpResponse<String> hubTypePostResp = hc.send(picoTypePost, HttpResponse.BodyHandlers.ofString());
            String[] locParts = hubTypePostResp.headers().firstValue("location").orElseThrow().split("/");
            if (locParts.length < 1) {
                throw new IOException("Missing pico type location URI");
            }
            id = Integer.parseInt(locParts[locParts.length - 1]);
        } else {
            id = picoType.getPicoEventTypeId();
        }

        CubelabAuditEntityDto dto = new CubelabAuditEntityDto(0, LocalDateTime.now().toString(), id, 1);

        HttpRequest msqReq = HttpRequest
            .newBuilder(getHostURI("/api/audit"))
            .POST(HttpRequest.BodyPublishers.ofString(toJsonString(dto)))
            .build();

        return hc.send(msqReq, HttpResponse.BodyHandlers.ofString());
    }

    public static List<CaseParamsEntityDto> getQueuedCases(@NotNull HttpClient hc)
        throws IOException, InterruptedException {
        HttpRequest caseStatesQueued = HttpRequest
            .newBuilder(getHostURI("/api/case-status/status/queued"))
            .GET()
            .build();
        String statesJson = hc.send(caseStatesQueued, HttpResponse.BodyHandlers.ofString()).body();
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> states = (List<Map<String, Object>>) fromJsonString(statesJson, listOfMapType());

        return states.stream()
            .map(state -> {
                HttpRequest params = HttpRequest
                    .newBuilder(getHostURI("/api/case-params/" + ((Double) state.get("caseId")).intValue()))
                    .GET()
                    .build();

                try {
                    String paramJson = hc.send(params, HttpResponse.BodyHandlers.ofString()).body();
                    return fromJsonString(paramJson, CaseParamsEntityDto.class);
                } catch (IOException | InterruptedException e) {
                    log.error("Failed to request case params for case " + ((Double) state.get("caseId")).intValue(), e);
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .toList();
    }

    public static CaseStatusEntityDto getCaseStatus(@NotNull HttpClient hc, int caseId)
        throws IOException, InterruptedException {
        HttpRequest caseStateGet = HttpRequest
            .newBuilder(getHostURI("/api/case-status/" + caseId))
            .GET()
            .build();

        String json = hc.send(caseStateGet, HttpResponse.BodyHandlers.ofString()).body();
        return fromJsonString(json, CaseStatusEntityDto.class);
    }

    public static void setCaseStatus(@NotNull HttpClient hc, int caseId, int statusId)
        throws IOException, InterruptedException {
        CaseStatusEntityDto state = getCaseStatus(hc, caseId);
        state.setCaseStatusId(statusId);

        HttpRequest caseStateSet = HttpRequest
            .newBuilder(getHostURI("/api/case-status/" + caseId))
            .method("PATCH", HttpRequest.BodyPublishers.ofString(toJsonString(state)))
            .build();

        hc.send(caseStateSet, HttpResponse.BodyHandlers.ofString());
    }

    public static void setCaseStatus(@NotNull HttpClient hc, int caseId, String status)
        throws IOException, InterruptedException {
        HttpRequest statusIdGet = HttpRequest
            .newBuilder(getHostURI("/api/case-status/statusType/" + status.toLowerCase()))
            .GET()
            .build();

        String statusTypeJson = hc.send(statusIdGet, HttpResponse.BodyHandlers.ofString()).body();
        CaseStatusesEntityDto statusType = fromJsonString(statusTypeJson, CaseStatusesEntityDto.class);
        int statusId = statusType.getCaseStatusId();

        setCaseStatus(hc, caseId, statusId);
    }

    public static List<CaptureTypesEntityDto> getCaptureTypes(@NotNull HttpClient hc)
        throws IOException, InterruptedException {
        HttpRequest captureTypesGet = HttpRequest
            .newBuilder(getHostURI("/api/case-params/capture-types"))
            .GET()
            .build();

        String captureTypesJson = hc.send(captureTypesGet, HttpResponse.BodyHandlers.ofString()).body();
        // We want to fail if type mismatch
        //noinspection unchecked
        return (List<CaptureTypesEntityDto>) fromJsonString(captureTypesJson, listOfType(CaptureTypesEntityDto.class));
    }

    public static List<CaseStatusesEntityDto> getStatusTypes(HttpClient hc) throws IOException, InterruptedException {
        HttpRequest statusTypesGet = HttpRequest
            .newBuilder(getHostURI("/api/case-status/statusTypes"))
            .GET()
            .build();

        String statusTypesJson = hc.send(statusTypesGet, HttpResponse.BodyHandlers.ofString()).body();
        // We want to fail if type mismatch
        //noinspection unchecked
        return (List<CaseStatusesEntityDto>) fromJsonString(statusTypesJson, listOfType(CaseStatusesEntityDto.class));
    }

    public static CaseStatusEntityDto setCaseCompleteTimestamp(HttpClient hc, Integer caseId, LocalDateTime ts)
        throws IOException, InterruptedException {

        CaseStatusEntityDto dto = getCaseStatus(hc, caseId);
        dto.setCompleteTimestamp(ts);

        HttpRequest patchCaseStatus = HttpRequest
            .newBuilder(getHostURI("/api/case-status/" + caseId))
            .method("PATCH", HttpRequest.BodyPublishers.ofString(toJsonString(dto)))
            .build();

        String statusResponseJson = hc.send(patchCaseStatus, HttpResponse.BodyHandlers.ofString()).body();
        return fromJsonString(statusResponseJson, CaseStatusEntityDto.class);
    }

    public static CaseStatusEntityDto setCaseStartTimestamp(HttpClient hc, Integer caseId, LocalDateTime ts)
        throws IOException, InterruptedException {
        CaseStatusEntityDto dto = getCaseStatus(hc, caseId);
        dto.setStartTimestamp(ts);

        HttpRequest patchCaseStatus = HttpRequest
            .newBuilder(getHostURI("/api/case-status/" + caseId))
            .method("PATCH", HttpRequest.BodyPublishers.ofString(toJsonString(dto)))
            .build();

        String statusResponseJson = hc.send(patchCaseStatus, HttpResponse.BodyHandlers.ofString()).body();
        return fromJsonString(statusResponseJson, CaseStatusEntityDto.class);
    }

    public static boolean postFootage(HttpClient hc, CaseFootageEntityDto dto)
        throws IOException, InterruptedException {
        HttpRequest postFootage = HttpRequest
            .newBuilder(getHostURI("/api/case-footage"))
            .POST(HttpRequest.BodyPublishers.ofString(toJsonString(dto)))
            .build();

        HttpResponse<String> resp = hc.send(postFootage, HttpResponse.BodyHandlers.ofString());
        return resp.statusCode() >= 200 && resp.statusCode() < 300;
    }

    public static URI getHostURI(String path) {
        if (path.trim().startsWith("/")) {
            return URI.create("http://" + CaseCoordinator.getInstance().getApiHost() + path);
        } else {
            return URI.create("http://" + CaseCoordinator.getInstance().getApiHost() + "/" + path);
        }
    }

    public static TypeToken<?> mapType() {
        return TypeToken.getParameterized(Map.class, String.class, Object.class);
    }

    public static TypeToken<?> listOfMapType() {
        return TypeToken.getParameterized(List.class, mapType().getType());
    }

    public static TypeToken<?> listOfType(Class<?> type) {
        return TypeToken.getParameterized(List.class, type);
    }
}
