/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package edu.udel.vanburenlabs.cubelab.casecoord.scripts;

import edu.udel.vanburenlabs.cubelab.casecoord.CaseCoordinator;
import edu.udel.vanburenlabs.cubelab.casecoord.util.Pair;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;
import java.util.Timer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
public class BurstCaptureWrapper extends ExecWrapper {
    private final String genTLLocation =
        String.valueOf(CaseCoordinator.getInstance().getConfig().get("spinnaker-gentl-location"));

    public BurstCaptureWrapper(String execPath) {
        super(execPath);
    }

    @SuppressWarnings("DuplicatedCode")
    public Optional<String> capture(int cameraId, double delay, int nBurst, int nFrames, String targetPath)
        throws IOException, InterruptedException, ExecutionException {
        ProcessBuilder processBuilder = new ProcessBuilder(execPath,
            String.valueOf(cameraId),
            String.valueOf(delay),
            String.valueOf(nBurst),
            String.valueOf(nFrames),
            targetPath);

        log.debug("Executing command: {}", String.join(" ", processBuilder.command()));

        processBuilder.environment().put("SPINNAKER_GENTL64_CTI", genTLLocation);

        Process proc = processBuilder.start();

        long pid = proc.pid();
        log.debug("Waiting for pid {}", pid);

        Pair<Timer, CompletableFuture<Pair<StringBuffer, StringBuffer>>> out =
            handleBuffers(proc, "burstCaptureTimer", true);

        CompletableFuture<Pair<StringBuffer, StringBuffer>> future = out.second();

        Pair<StringBuffer, StringBuffer> outAndErr = future.get();

        StringBuffer stdout = outAndErr.first();
        StringBuffer stderr = outAndErr.second();

        if (proc.exitValue() != 0) {
            captureFailMethod(proc, stderr);

            return Optional.empty();
        }

        String outString = stdout.toString();

        for (String section : outString.split("\n\n")) {
            section = section.trim();
            if (section.startsWith("/") && section.endsWith(".bz2")) {
                return Optional.of(section);
            }
        }

        log.warn("Could not find path to output file from capture.");
        return Optional.empty();
    }
}
