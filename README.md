﻿# CubeLab API and Management Apps

This repo contains a variety of apps and libraries for the OrangePi Single Board Computer (SBC) onboard the experiment. Here is a brief overview of each project subfolder in the repo.

Each project will have its own README to better explain its moving parts and requirements.

-----
## `/case-coordinator`
This is a Java (Maven) application that facilitates the capture of video data by the SBC during experiment cases. It interacts with the REST API to handle data and capture parameters from the database.

## `/datamodels`
This is a Java class library that holds all the JPA data model classes that match up with the Postgres Database schema. These classes are a requirement for most of the other apps in this repo. This project will need to be installed through Maven before most others can run.

## `/frontend`
This was a nice-to-have project that is not close to completion. We wanted a pretty frontend GUI to help remote user interaction with the CubeLab, but we didn't quite have time to get to it yet.

## `/picotunnel`
This small Java application interfaces with both the REST API and the Pico microcontroller to act as a translation layer between the server's HTTP protocol and the very basic Serial UART command protocol the Pico has. This app processes parameters sent by the REST server and forwards the commands to the Pico microcontroller.

## `/rest-api`
This is a Java Micronaut application, and the bulk of the code content of this repo. This is a local HTTP server application that handles requests from many of the other onboard applications, and serves as a connector to our internal Postgres Database. It handles all the database connections to streamline data capture and processing, and generates commands for the microcontroller so it can run the attached hardware.
