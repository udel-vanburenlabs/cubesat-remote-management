/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabPicoEventTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabPicoEventTypesEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CubelabPicoEventTypesMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class AuditPicoEventTypesDao {

    private final Query<CubelabPicoEventTypesEntity> allEventTypes;
    private final Query<CubelabPicoEventTypesEntity> byTypeId;
    private final Query<CubelabPicoEventTypesEntity> byTypeName;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public AuditPicoEventTypesDao() {
        allEventTypes = session.createQuery("from CubelabPicoEventTypesEntity", CubelabPicoEventTypesEntity.class);
        byTypeId = session.createQuery("from CubelabPicoEventTypesEntity where picoEventTypeId = ?1", CubelabPicoEventTypesEntity.class);
        byTypeName = session.createQuery("from CubelabPicoEventTypesEntity where picoEventType = ?1", CubelabPicoEventTypesEntity.class);
    }

    public Stream<CubelabPicoEventTypesEntityDto> getAll() {
        return allEventTypes
            .stream()
            .map(instance::entityToDTO);
    }

    public CubelabPicoEventTypesEntityDto getByTypeId(int typeId) {
        byTypeId.setParameter(1, typeId);

        return byTypeId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CubelabPicoEventTypesEntityDto getByTypeName(String name) {
        byTypeName.setParameter(1, name);

        return byTypeName
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CubelabPicoEventTypesEntity postType(CubelabPicoEventTypesEntityDto dto) {
        CubelabPicoEventTypesEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }
}
