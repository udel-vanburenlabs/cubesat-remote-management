/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.cases;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseFootageEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseFootageEntity;
import edu.udel.vanburenlabs.cubelab.restapi.dao.CaseFootageDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.server.types.files.StreamedFile;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Slf4j
@Controller("/api/case-footage")
public class CaseFootageController {

    @Inject
    private CaseFootageDao caseFootageDao;

    @Get(value = "/caseId/{caseId}/sequenceId/{sequenceId}/fileId/{fileId}/file", produces = MediaType.APPLICATION_OCTET_STREAM)
    public HttpResponse<StreamedFile> getFootage(int caseId, int sequenceId, int fileId) throws IOException {
        CaseFootageEntityDto dto = caseFootageDao.getFootageEntityByCaseIdAndSeqIdAndFileId(caseId, sequenceId, fileId);
        if (dto == null) {
            return HttpResponse.notFound();
        }

        String path = dto.getFilePath();
        if (path == null || path.isBlank()) {
            return HttpResponse.noContent();
        }

        return HttpResponse.ok(new StreamedFile(Files.newInputStream(Path.of(path)), MediaType.APPLICATION_OCTET_STREAM_TYPE));
    }

    @Get(value = "/caseId/{caseId}/sequenceId/{sequenceId}/fileId/{fileId}", produces = APPLICATION_JSON)
    public HttpResponse<String> getFootageMeta(int caseId, int sequenceId, int fileId) {
        CaseFootageEntityDto dto = caseFootageDao.getFootageEntityByCaseIdAndSeqIdAndFileId(caseId, sequenceId, fileId);
        if (dto == null) {
            return HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(dto));
    }

    @Get(produces = APPLICATION_JSON)
    public String getAll() {
        return toJsonString(caseFootageDao.getFootageEntities().toList());
    }

    @Get(value = "/caseId/{caseId}", produces = APPLICATION_JSON)
    public String getByCaseId(int caseId) {
        return toJsonString(caseFootageDao.getFootageEntitiesByCaseId(caseId).toList());
    }

    @Get(value = "/caseId/{caseId}/sequenceId/{sequenceId}", produces = APPLICATION_JSON)
    public String getByCaseIdAndSeqId(int caseId, int sequenceId) {
        return toJsonString(caseFootageDao.getFootageEntitiesByCaseIdAndSeqId(caseId, sequenceId).toList());
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<Void> postFootage(@Body String json) {
        CaseFootageEntity created;

        created = caseFootageDao.postFootageEntity(fromJsonString(json, CaseFootageEntityDto.class));

        return HttpResponse.created(URI.create("/api/case-footage/caseId/" + created.getCaseId() + "/sequenceId/" +
            created.getSequenceId() + "/fileId/" + created.getFileId()));
    }

    @Delete(value = "/caseId/{caseId}/sequenceId/{sequenceId}/fileId/{fileId}", produces = APPLICATION_JSON)
    public HttpResponse<String> deleteFootage(int caseId, int sequenceId, int fileId) {

        CaseFootageEntityDto dto = caseFootageDao.deleteFootageEntity(caseId, sequenceId, fileId);

        if (dto == null) {
            return HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(dto));
    }
}
