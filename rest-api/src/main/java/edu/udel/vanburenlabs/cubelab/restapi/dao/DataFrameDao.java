/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.MessagesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.PicoDataframesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.MessagesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.PicoDataframesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.mappers.MessagesMapper;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.PicoDataframesMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;

@Singleton
public class DataFrameDao {

    private final Query<PicoDataframesEntity> allDataFrames;
    private final Query<PicoDataframesEntity> dataFrameById;
    private final Query<PicoDataframesEntity> dataFrameByTimestampAndPicoId;
    private final Query<MessagesEntity> messageById;
    private final Query<PicoDataframesEntity> dataFramesByStartAndEnd;

    /**
     * {@link Query#setMaxResults(int)} is necessary for this query.
     */
    private final Query<PicoDataframesEntity> dataFramesByRecentN;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public DataFrameDao() {

        this.dataFrameById =
            session.createQuery("from PicoDataframesEntity where frameId = ?1", PicoDataframesEntity.class);
        this.allDataFrames =
            session.createQuery("from PicoDataframesEntity", PicoDataframesEntity.class);
        this.dataFrameByTimestampAndPicoId =
            session.createQuery("from PicoDataframesEntity where picoId = ?1 and timestamp = ?2", PicoDataframesEntity.class);
        this.dataFramesByStartAndEnd =
            session.createQuery("from PicoDataframesEntity where timestamp between ?1 and ?2", PicoDataframesEntity.class);
        this.dataFramesByRecentN =
            session.createQuery("from PicoDataframesEntity order by timestamp desc", PicoDataframesEntity.class);

        this.messageById =
            session.createQuery("from MessagesEntity where messageId = ?1", MessagesEntity.class);
    }

    public PicoDataframesEntityDto getDataFrame(int frameId) {
        return dataFrameById
            .setParameter(1, frameId)
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public Stream<PicoDataframesEntityDto> getDataFrames() {
        return allDataFrames
            .stream()
            .map(instance::entityToDTO);
    }

    public PicoDataframesEntityDto getDataFrame(int picoId, LocalDateTime timestamp) {
        dataFrameByTimestampAndPicoId.setParameter(1, timestamp.format(ISO_INSTANT));
        dataFrameByTimestampAndPicoId.setParameter(2, picoId);

        return dataFrameByTimestampAndPicoId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public Stream<PicoDataframesEntityDto> getDataFrames(LocalDateTime start, LocalDateTime end) {
        dataFramesByStartAndEnd.setParameter(1, start);
        dataFramesByStartAndEnd.setParameter(2, end);

        return dataFramesByStartAndEnd
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<PicoDataframesEntityDto> getDataFrames(int picoId, LocalDateTime start, LocalDateTime end) {
        return getDataFrames(start, end).filter(uartDataPacket -> uartDataPacket.getPicoId() == picoId);
    }

    public Stream<PicoDataframesEntityDto> getDataFrames(int n) {
        dataFramesByRecentN.setMaxResults(n);

        return dataFramesByRecentN
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<PicoDataframesEntityDto> getDataFrames(int picoId, int n) {
        return getDataFrames(n).filter(uartDataPacket -> uartDataPacket.getPicoId() == picoId);
    }

    public String getMessageForFrameId(int frameId) {
        int messageId = getDataFrame(frameId).getMessageId();

        return getMessageById(messageId);
    }

    public MessagesEntity putMessage(String message) {
        MessagesEntity entity = MessagesMapper.instance.DTOToEntity(new MessagesEntityDto(0, message));
        inTransaction(session,
            () -> session.insert(entity)
        );

        return entity;
    }

    public String getMessageById(int messageId) {
        messageById.setParameter(1, messageId);

        return messageById
            .stream()
            .map(MessagesMapper.instance::entityToDTO)
            .map(MessagesEntityDto::getMessageText)
            .findAny()
            .orElse("");
    }

    public PicoDataframesEntity putDataframe(PicoDataframesEntityDto dto) {
        PicoDataframesEntity entity = instance.DTOToEntity(dto);

        inTransaction(session,
            () -> session.insert(entity)
        );

        return entity;
    }
}
