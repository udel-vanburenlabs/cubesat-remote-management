/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseLogEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseLogEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseLogMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class CaseLogDao {
    private final Query<CaseLogEntity> allLogEntities;
    private final Query<CaseLogEntity> logEntityByCaseEventId;
    private final Query<CaseLogEntity> logEntityByCaseId;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public CaseLogDao() {
        allLogEntities = session.createQuery("from CaseLogEntity ", CaseLogEntity.class);
        logEntityByCaseEventId = session.createQuery("from CaseLogEntity where caseId = ?1 and eventId = ?2", CaseLogEntity.class);
        logEntityByCaseId = session.createQuery("from CaseLogEntity where caseId = ?1", CaseLogEntity.class);
    }

    public Stream<CaseLogEntityDto> getLogEntities() {
        return allLogEntities
            .stream()
            .map(instance::entityToDTO);
    }

    @Nullable
    public CaseLogEntityDto getLogEntityByCaseEventId(int caseId, int eventId) {
        logEntityByCaseEventId.setParameter(1, caseId);
        logEntityByCaseEventId.setParameter(2, eventId);

        return logEntityByCaseEventId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public Stream<CaseLogEntityDto> getLogEntitiesByCaseId(int caseId) {
        logEntityByCaseId.setParameter(1, caseId);

        return logEntityByCaseId
            .stream()
            .map(instance::entityToDTO);
    }

    public CaseLogEntity postCaseLogEntity(CaseLogEntityDto dto) {
        CaseLogEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }
}
