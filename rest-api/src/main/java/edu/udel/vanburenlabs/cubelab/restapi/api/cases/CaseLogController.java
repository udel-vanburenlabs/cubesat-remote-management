/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.cases;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseLogEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseLogEntity;
import edu.udel.vanburenlabs.cubelab.restapi.dao.CaseLogDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;

import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Slf4j
@Controller("/api/case-log")
public class CaseLogController {
    @Inject
    private CaseLogDao caseLogDao;

    @Get(produces = APPLICATION_JSON)
    public String all() {
        return toJsonString(caseLogDao.getLogEntities().toList());
    }

    @Get(value = "/caseId/{caseId}", produces = APPLICATION_JSON)
    public String byCaseId(int caseId) {
        return toJsonString(caseLogDao.getLogEntitiesByCaseId(caseId).toList());
    }

    @Get(value = "/caseId/{caseId}/eventId/{eventId}", produces = APPLICATION_JSON)
    public HttpResponse<String> byCaseIdAndEventId(int caseId, int eventId) {
        CaseLogEntityDto dto = caseLogDao.getLogEntityByCaseEventId(caseId, eventId);

        if (dto == null) {
            HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(dto));
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<Void> postCaseLog(@Body String json) {
        CaseLogEntity created;

        created = caseLogDao.postCaseLogEntity(fromJsonString(json, CaseLogEntityDto.class));

        return HttpResponse.created(URI.create("/api/case-log/caseId/" + created.getCaseId() + "/eventId/" + created.getEventId()));
    }
}
