/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseStatusEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseStatusesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseStatusTypesMapper;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseStatusMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class CaseStatusDao {
    private final Query<CaseStatusEntity> allCaseStatuses;
    private final Query<CaseStatusEntity> caseStatusById;
    private final Query<CaseStatusEntity> caseStatusByStatusId;
    private final Query<CaseStatusesEntity> caseStatusByStatusName;
    private final Query<CaseStatusesEntity> allCaseStatusTypes;
    private final StatelessSession session = HibernateConfigurator.getSession();

    public CaseStatusDao() {

        this.allCaseStatuses =
            session.createQuery("from CaseStatusEntity order by caseId asc", CaseStatusEntity.class);

        this.caseStatusById =
            session.createQuery("from CaseStatusEntity where caseId = ?1", CaseStatusEntity.class);

        this.caseStatusByStatusId =
            session.createQuery("from CaseStatusEntity where caseStatusId = ?1 order by caseId asc", CaseStatusEntity.class);

        this.caseStatusByStatusName =
            session.createQuery("from CaseStatusesEntity where caseStatus = ?1", CaseStatusesEntity.class);

        this.allCaseStatusTypes =
            session.createQuery("from CaseStatusesEntity", CaseStatusesEntity.class);
    }

    public Stream<CaseStatusEntityDto> getCaseStatus() {
        return allCaseStatuses
            .stream()
            .map(instance::entityToDTO);
    }

    @Nullable
    public CaseStatusEntityDto getCaseStatus(int caseId) {
        caseStatusById.setParameter(1, caseId);

        return caseStatusById
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public Stream<CaseStatusEntityDto> getByStatusId(int statusId) {
        caseStatusByStatusId.setParameter(1, statusId);

        return caseStatusByStatusId
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<CaseStatusEntityDto> getByStatusName(String statusName) {
        caseStatusByStatusName.setParameter(1, statusName);

        CaseStatusesEntity status = caseStatusByStatusName
            .stream()
            .findFirst()
            .orElse(null);
        if (status != null) {
            return getByStatusId(status.getCaseStatusId());
        }

        return Stream.empty();
    }

    public CaseStatusesEntityDto getStatusIdByName(String statusName) {
        caseStatusByStatusName.setParameter(1, statusName);

        return caseStatusByStatusName
            .stream()
            .map(CaseStatusTypesMapper.instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CaseStatusEntityDto postCaseStatus(CaseStatusEntityDto dto) {
        CaseStatusEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return instance.entityToDTO(entity);
    }

    public Stream<CaseStatusesEntityDto> allStatusTypes() {
        return allCaseStatusTypes
            .stream()
            .map(CaseStatusTypesMapper.instance::entityToDTO);
    }

    public boolean updateCaseStatus(CaseStatusEntityDto dto) {
        caseStatusById.setParameter(1, dto.getCaseId());

        CaseStatusEntity entity = caseStatusById
            .stream()
            .findFirst()
            .orElse(null);

        instance.updateEntity(entity, dto);

        return inTransaction(session, () -> session.update(entity));
    }

    public Stream<CaseStatusesEntityDto> getAllCaseStatusTypes() {
        return allCaseStatusTypes
            .stream()
            .map(CaseStatusTypesMapper.instance::entityToDTO);
    }

    public int getCurrentCaseId() {
        Optional<CaseStatusEntityDto> result = getCaseStatus()
            .filter(
                dto -> getAllCaseStatusTypes()
                    .filter(type -> type.getCaseStatusId() == dto.getCaseStatusId())
                    .anyMatch(type -> type.getCaseStatus().equals("in_progress"))
            )
            .findFirst();

        if (result.isPresent()) {
            return result.get().getCaseId();
        }

        return -1;
    }

    public int getPrevCaseId(int flowFacility, CaseParamsDao caseParamsDao) {
        Optional<CaseStatusEntityDto> result = getCaseStatus()
            .filter(
                dto -> getAllCaseStatusTypes()
                    .filter(type -> type.getCaseStatusId() == dto.getCaseStatusId())
                    .anyMatch(type -> {
                        String t = type.getCaseStatus();
                        return t.equals("failed") || t.equals("complete");
                    })
            )
            .filter(dto -> {
                CaseParamsEntityDto params = caseParamsDao.getCaseParams(dto.getCaseId());
                if (params == null) {
                    return false;
                }
                return params.getFlowFacility() == flowFacility;
            })
            .reduce((dto1, dto2) -> {
                if (dto1.getCaseStatusId() >= dto2.getCaseStatusId()) {
                    return dto1;
                } else {
                    return dto2;
                }
            });

        if (result.isPresent()) {
            return result.get().getCaseId();
        }

        return -1;
    }

    public int getNextCaseId(int flowFacility, CaseParamsDao caseParamsDao) {
        Optional<CaseStatusEntityDto> result = getCaseStatus()
            .filter(
                dto -> getAllCaseStatusTypes()
                    .filter(type -> type.getCaseStatusId() == dto.getCaseStatusId())
                    .anyMatch(type -> type.getCaseStatus().equals("queued"))
            )
            .filter(dto -> {
                CaseParamsEntityDto params = caseParamsDao.getCaseParams(dto.getCaseId());
                if (params == null) {
                    return false;
                }
                return params.getFlowFacility() == flowFacility;
            })
            .reduce((dto1, dto2) -> {
                if (dto1.getCaseStatusId() >= dto2.getCaseStatusId()) {
                    return dto1;
                } else {
                    return dto2;
                }
            });

        if (result.isPresent()) {
            return result.get().getCaseId();
        }

        return -1;
    }

    public int getPrevCaseId() {
        Optional<CaseStatusEntityDto> result = getCaseStatus()
            .filter(
                dto -> getAllCaseStatusTypes()
                    .filter(type -> type.getCaseStatusId() == dto.getCaseStatusId())
                    .anyMatch(type -> {
                        String t = type.getCaseStatus();
                        return t.equals("failed") || t.equals("complete");
                    })
            )
            .reduce((dto1, dto2) -> {
                if (dto1.getCaseStatusId() >= dto2.getCaseStatusId()) {
                    return dto1;
                } else {
                    return dto2;
                }
            });

        if (result.isPresent()) {
            return result.get().getCaseId();
        }

        return -1;
    }

    public int getNextCaseId() {
        Optional<CaseStatusEntityDto> result = getCaseStatus()
            .filter(
                dto -> getAllCaseStatusTypes()
                    .filter(type -> type.getCaseStatusId() == dto.getCaseStatusId())
                    .anyMatch(type -> type.getCaseStatus().equals("queued"))
            )
            .reduce((dto1, dto2) -> {
                if (dto1.getCaseStatusId() >= dto2.getCaseStatusId()) {
                    return dto1;
                } else {
                    return dto2;
                }
            });

        if (result.isPresent()) {
            return result.get().getCaseId();
        }

        return -1;
    }
}
