/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseFootageEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseFootageEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseFootageMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class CaseFootageDao {
    private final Query<CaseFootageEntity> allFootageEntities;
    private final Query<CaseFootageEntity> footageEntitiesByCaseId;
    private final Query<CaseFootageEntity> footageEntitiesByCaseSequenceId;
    private final Query<CaseFootageEntity> footageEntityByCaseSeqFileId;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public CaseFootageDao() {
        allFootageEntities = session.createQuery("from CaseFootageEntity ", CaseFootageEntity.class);
        footageEntitiesByCaseId = session.createQuery("from CaseFootageEntity where caseId = ?1", CaseFootageEntity.class);
        footageEntitiesByCaseSequenceId = session.createQuery("from CaseFootageEntity where caseId = ?1 and sequenceId = ?2", CaseFootageEntity.class);
        footageEntityByCaseSeqFileId = session.createQuery("from CaseFootageEntity where caseId = ?1 and sequenceId = ?2 and fileId = ?3", CaseFootageEntity.class);
    }

    public Stream<CaseFootageEntityDto> getFootageEntities() {
        return allFootageEntities
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<CaseFootageEntityDto> getFootageEntitiesByCaseId(int caseId) {
        footageEntitiesByCaseId.setParameter(1, caseId);

        return footageEntitiesByCaseId
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<CaseFootageEntityDto> getFootageEntitiesByCaseIdAndSeqId(int caseId, int sequenceId) {
        footageEntitiesByCaseSequenceId.setParameter(1, caseId);
        footageEntitiesByCaseSequenceId.setParameter(2, sequenceId);

        return footageEntitiesByCaseSequenceId
            .stream()
            .map(instance::entityToDTO);
    }

    @Nullable
    public CaseFootageEntityDto getFootageEntityByCaseIdAndSeqIdAndFileId(int caseId, int sequenceId, int fileId) {
        footageEntityByCaseSeqFileId.setParameter(1, caseId);
        footageEntityByCaseSeqFileId.setParameter(2, sequenceId);
        footageEntityByCaseSeqFileId.setParameter(3, fileId);

        return footageEntityByCaseSeqFileId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CaseFootageEntity postFootageEntity(CaseFootageEntityDto dto) {
        CaseFootageEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }

    public CaseFootageEntityDto deleteFootageEntity(int caseId, int sequenceId, int fileId) {
        CaseFootageEntityDto dto = getFootageEntityByCaseIdAndSeqIdAndFileId(caseId, sequenceId, fileId);
        CaseFootageEntity entity = instance.DTOToEntity(dto);
        inTransaction(session, () -> session.delete(entity));
        return dto;
    }
}
