/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.util;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * Various data serialization/deserialization utilities using Google Gson.
 */
public class JsonUtils {
    /**
     * System Gson ser/deserializer. Has multiple type adapters to handle special types.
     */
    private static final Gson gson = new Gson()
        .newBuilder()
        .registerTypeAdapter(LocalDateTime.class, new TypeAdapter<LocalDateTime>() {

            @Override
            public void write(JsonWriter out, LocalDateTime value) throws IOException {
                if (value == null) {
                    out.value((String) null);
                } else {
                    out.value(value.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS")));
                }
            }

            @Override
            public LocalDateTime read(JsonReader in) throws IOException {
                String value = in.nextString();
                return LocalDateTime.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS"));
            }
        })
        .registerTypeAdapter(TimeZone.class, new TypeAdapter<TimeZone>() {
            @Override
            public void write(JsonWriter out, TimeZone value) throws IOException {
                if (value == null) {
                    out.value((String) null);
                } else {
                    out.value(value.getID());
                }
            }

            @Override
            public TimeZone read(JsonReader in) throws IOException {
                String value = in.nextString();
                return TimeZone.getTimeZone(value);
            }
        })
        .registerTypeAdapter(ZoneOffset.class, new TypeAdapter<ZoneOffset>() {
            @Override
            public void write(JsonWriter out, ZoneOffset value) throws IOException {
                if (value == null) {
                    out.value((String) null);
                } else {
                    out.value(value.getId());
                }
            }

            @Override
            public ZoneOffset read(JsonReader in) throws IOException {
                String value = in.nextString();
                return ZoneOffset.of(value);
            }
        })
        .registerTypeAdapter(LocalDate.class, new TypeAdapter<LocalDate>() {

            @Override
            public void write(JsonWriter out, LocalDate value) throws IOException {
                if (value == null) {
                    out.value((String) null);
                } else {
                    out.value(value.format(DateTimeFormatter.ISO_DATE));
                }
            }

            @Override
            public LocalDate read(JsonReader in) throws IOException {
                String value = in.nextString();
                return LocalDate.ofInstant(Instant.parse(value), ZoneOffset.UTC);
            }
        })
        .create();

    /**
     * Helper method for converting objects to JSON using the system serializer.
     * @return the JSON data
     */
    public static String toJsonString(Object src) {
        return gson.toJson(src);
    }

    /**
     * Helper method for converting JSON data into their model objects.
     * @param json a String containing JSON data
     * @param type the desired type to instantiate from the data
     * @return an instance of the type specified, holding the data provided
     */
    public static <T> T fromJsonString(String json, Class<? extends T> type) {
        return gson.fromJson(json, type);
    }

    /**
     * Helper method for converting JSON data into their model objects.
     * Use this method for converting to types with generics.
     * @param json a String containing JSON data
     * @param type the desired type to instantiate from the data
     * @return an instance of the type specified, holding the data provided
     */
    public static <T> T fromJsonString(String json, TypeToken<? extends T> type) {
        return gson.fromJson(json, type);
    }

}
