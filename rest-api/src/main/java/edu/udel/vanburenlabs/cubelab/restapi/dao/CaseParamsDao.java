/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaptureTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaptureTypesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseParamsEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaptureTypesMapper;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseParamsMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;
import static java.text.MessageFormat.format;

@Slf4j
@Singleton
public class CaseParamsDao {
    private final Query<CaseParamsEntity> allCaseParams;
    private final Query<CaseParamsEntity> caseParamsById;
    private final Query<CaptureTypesEntity> allCaptureTypes;

    private final StatelessSession session = HibernateConfigurator.getSession();

    @Inject
    @Getter
    private CaseStatusDao caseStatusDao;

    public CaseParamsDao() {
        this.allCaseParams =
            session.createQuery("from CaseParamsEntity order by caseId asc", CaseParamsEntity.class);

        this.caseParamsById =
            session.createQuery("from CaseParamsEntity where caseId = ?1", CaseParamsEntity.class);

        this.allCaptureTypes =
            session.createQuery("from CaptureTypesEntity", CaptureTypesEntity.class);
    }

    public Stream<CaseParamsEntityDto> getCaseParams() {
        return allCaseParams
            .stream()
            .map(instance::entityToDTO);
    }

    @Nullable
    public CaseParamsEntityDto getCaseParams(int caseId) {
        caseParamsById.setParameter(1, caseId);

        return caseParamsById
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CaseParamsEntity postCaseParams(CaseParamsEntityDto dto) {
        CaseParamsEntity entity =  instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }

    public Stream<CaptureTypesEntityDto> getCaptureTypes() {
        return allCaptureTypes
            .stream()
            .map(CaptureTypesMapper.instance::entityToDTO);
    }

    public boolean updateCaseParams(CaseParamsEntityDto dto) {
        return inTransaction(session, () -> session.update(instance.DTOToEntity(dto)));
    }

    public static List<String> getPicoResetCommands() {
        return List.of(
            "SET_LASER 0 0",
            "SET_LASER 1 0",
            "SET_STEPPER 0 0",
            "SET_STEPPER 1 0",
            "FLUSH_DATA"
        );
    }

    public Stream<String> getPicoCommandsForCase(int caseId) {
        CaseParamsEntityDto dto = getCaseParams(caseId);

        if (dto == null) {
            return Stream.empty();
        }

        int flowFacility = dto.getFlowFacility();

        ArrayList<String> commands = new ArrayList<>(getPicoResetCommands());

        commands.add("START_CASE " + caseId);

        int lastCaseId = caseStatusDao.getPrevCaseId(flowFacility, this);

        CaseStatusEntityDto prev = (lastCaseId > 0) ? caseStatusDao.getCaseStatus(lastCaseId) : null;

        if (prev != null) {
            long secSinceLast = ChronoUnit.SECONDS.between(prev.getCompleteTimestamp(), LocalDateTime.now());

            if (dto.getMinSecSinceLast() > secSinceLast) {
                long secToWait = secSinceLast - dto.getMinSecSinceLast();
                commands.add("WAIT " + secToWait);
            }
        }

        if (dto.getMinTemp() != 0) {
            commands.add("MIN_TEMP " + dto.getMinTemp());
        }

        if (dto.getMaxTemp() != 0) {
            commands.add("MAX_TEMP " + dto.getMaxTemp());
        }

        commands.add(format("SET_LASER {0} {1}", flowFacility, dto.getLaserBrightness()));

        commands.add(format("SET_STEPPER {0} {1}", flowFacility, dto.getStepperSpeed()));

        commands.add("WAIT " + dto.getSpinDurationSec());

        commands.add(format("SET_STEPPER {0} 0", flowFacility));

        int additionalWaitSec = dto.getCaptureDurationSec() - dto.getSpinDurationSec();

        if (additionalWaitSec > 0) {
            commands.add("WAIT " + additionalWaitSec);
        }

        commands.add("END_CASE " + caseId);

        return commands.stream();
    }
}
