/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.util;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;

import javax.annotation.Nullable;
import java.util.concurrent.Callable;

/**
 * Various utilities for using SQL transactions.
 */
@Slf4j
public class TransactionUtils {

    /**
     * Wraps a procedure with a transaction to ensure atomicity.
     * @param s the Hibernate session to be used
     * @param transactionSupplier a lambda or function to be called within the transaction
     * @return the result of the transaction, if any.
     */
    @Nullable
    public static <T> T inTransaction(StatelessSession s, Callable<? extends T> transactionSupplier) {
        Transaction t = s.beginTransaction();

        T result = null;
        try {
            result = transactionSupplier.call();
            t.commit();
        } catch (Exception e) {
            t.rollback();
            log.error("Failed to apply action: ", e);
        }

        return result;
    }

    /**
     * Wraps a procedure with a transaction to ensure atomicity.
     * @param s the Hibernate session to be used
     * @param transactionSupplier a lambda or function to be called within the transaction
     * @return true if the transaction was successful (committed), or false otherwise (rolled back).
     */
    public static boolean inTransaction(StatelessSession s, Runnable transactionSupplier) {
        Transaction t = s.beginTransaction();

        try {
            transactionSupplier.run();
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            log.error("Failed to apply action: ", e);
        }

        return false;
    }
}
