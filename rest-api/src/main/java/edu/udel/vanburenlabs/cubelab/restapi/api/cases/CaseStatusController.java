/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.cases;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseStatusesEntityDto;
import edu.udel.vanburenlabs.cubelab.restapi.dao.CaseStatusDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Patch;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseStatusMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Slf4j
@Controller("/api/case-status")
public class CaseStatusController {

    @Inject
    private CaseStatusDao caseStatusDao;

    @Get(produces = APPLICATION_JSON)
    public String all() {
        return toJsonString(caseStatusDao.getCaseStatus().toList());
    }

    @Get(value = "/{caseId}/", produces = APPLICATION_JSON)
    public HttpResponse<String> fromCaseId(int caseId) {
        CaseStatusEntityDto dto = caseStatusDao.getCaseStatus(caseId);
        if (dto == null) {
            return HttpResponse.notFound();
        }
        return HttpResponse.ok(toJsonString(dto));
    }

    @Get(value = "/status/{status}", produces = APPLICATION_JSON)
    public String fromStatus(String status) {
        return toJsonString(caseStatusDao.getByStatusName(status).toList());
    }

    @Get(value = "/statusType/{status}", produces = APPLICATION_JSON)
    public HttpResponse<String> statusTypefromStatus(String status) {
        CaseStatusesEntityDto dto = caseStatusDao.getStatusIdByName(status);

        if (dto == null) {
            return HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(dto));
    }

    @Get(value = "/statusTypes", produces = APPLICATION_JSON)
    public String getStatusTypes() {
        return toJsonString(caseStatusDao.allStatusTypes().toList());
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<Void> postCaseStatus(@Body String json) {
        CaseStatusEntityDto created;

        created = caseStatusDao.postCaseStatus(fromJsonString(json, CaseStatusEntityDto.class));

        return HttpResponse.created(URI.create("/api/case-status/" + created.getCaseId()));
    }

    @Patch(value = "/{caseId}/", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
    public HttpResponse<String> putCaseStatus(int caseId, @Body String json) {
        CaseStatusEntityDto dto = caseStatusDao.getCaseStatus(caseId);

        if (dto == null) {
            return HttpResponse.notFound();
        }

        instance.updateDTO(dto, fromJsonString(json, CaseStatusEntityDto.class));

        caseStatusDao.updateCaseStatus(dto);
        return HttpResponse.ok(toJsonString(dto));
    }
}
