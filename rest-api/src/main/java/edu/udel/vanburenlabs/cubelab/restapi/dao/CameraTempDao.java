/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CameraTempEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CameraTempEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CameraTempMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class CameraTempDao {
    private final Query<CameraTempEntity> allTempEntities;
    private final Query<CameraTempEntity> tempEntityById;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public CameraTempDao() {
        allTempEntities = session.createQuery("from CameraTempEntity order by timestamp desc", CameraTempEntity.class);
        tempEntityById = session.createQuery("from CameraTempEntity where id = ?1", CameraTempEntity.class);
    }

    public Stream<CameraTempEntityDto> getTempObjects() {
        return allTempEntities
            .stream()
            .map(instance::entityToDTO);
    }

    public CameraTempEntity postTempObject(CameraTempEntityDto dto) {
        CameraTempEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }

    @Nullable
    public CameraTempEntityDto getTempObject(int id) {
        tempEntityById.setParameter(1, id);

        return tempEntityById
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }
}
