/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.cases;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CaseParamsEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CaseParamsEntity;
import edu.udel.vanburenlabs.cubelab.restapi.dao.CaseParamsDao;
import edu.udel.vanburenlabs.cubelab.restapi.dao.CaseStatusDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Patch;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CaseParamsMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Slf4j
@Controller("/api/case-params")
public class CaseParamsController {

    @Inject
    CaseParamsDao caseParamsDao;

    @Get(produces = APPLICATION_JSON)
    public String all() {
        return toJsonString(caseParamsDao.getCaseParams().toList());
    }

    @Get(value = "/{caseId}/", produces = APPLICATION_JSON)
    public HttpResponse<String> fromCaseId(int caseId) {
        CaseParamsEntityDto dto = caseParamsDao.getCaseParams(caseId);

        if (dto == null) {
            return HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(dto));
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<Void> postCaseParams(@Body String json) {
        CaseParamsEntity created;

        created = caseParamsDao.postCaseParams(fromJsonString(json, CaseParamsEntityDto.class));

        return HttpResponse.created(URI.create("/api/case-params/" + created.getCaseId()));
    }

    @Patch(value = "/{caseId}/", consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
    public HttpResponse<String> putCaseParams(int caseId, @Body String json) {
        CaseParamsEntityDto dto = caseParamsDao.getCaseParams(caseId);

        if (dto == null) {
            return HttpResponse.notFound();
        }

        instance.updateDTO(dto, fromJsonString(json, CaseParamsEntityDto.class));

        if (caseParamsDao.updateCaseParams(dto)) {
            return HttpResponse.ok(toJsonString(dto));
        } else {
            return HttpResponse.badRequest("{\"error\":\"Could not update Case parameters id: " + dto.getCaseId() + "\"}");
        }
    }

    @Get(value = "/capture-types/", produces = APPLICATION_JSON)
    public String getCaptureTypes() {
        return toJsonString(caseParamsDao.getCaptureTypes().toList());
    }

    @Get(value = "/commands/byMcu/{picoId}/", produces = APPLICATION_JSON)
    public HttpResponse<String> getPicoCommands(int picoId) {
        if (picoId > 1) {
            return HttpResponse.notFound();
        }

        CaseStatusDao statusDao = caseParamsDao.getCaseStatusDao();
        int caseId = statusDao.getNextCaseId(picoId, caseParamsDao);
        if (caseId < 0) {
            return HttpResponse.noContent();
        }

        return HttpResponse.ok(toJsonString(caseParamsDao.getPicoCommandsForCase(caseId).toList()));
    }

    @Get(value = "/commands/byId/{caseId}/", produces = APPLICATION_JSON)
    public HttpResponse<String> getPicoCommandsByCaseId(int caseId) {
        if (caseParamsDao.getCaseParams(caseId) == null) {
            return HttpResponse.notFound();
        }

        return HttpResponse.ok(toJsonString(caseParamsDao.getPicoCommandsForCase(caseId).toList()));
    }

    @Get(value = "/next/{picoId}", produces = APPLICATION_JSON)
    public HttpResponse<String> getNext(int picoId) {
        int nextId = caseParamsDao.getCaseStatusDao().getNextCaseId(picoId, caseParamsDao);

        if (nextId < 0) {
            return HttpResponse.noContent();
        }

        return fromCaseId(nextId);
    }

    @Get(value = "/prev/{picoId}", produces = APPLICATION_JSON)
    public HttpResponse<String> getPrev(int picoId) {
        int prevId = caseParamsDao.getCaseStatusDao().getPrevCaseId(picoId, caseParamsDao);

        if (prevId < 0) {
            return HttpResponse.noContent();
        }

        return fromCaseId(prevId);
    }

    @Get(value = "/next", produces = APPLICATION_JSON)
    public HttpResponse<String> getNext() {
        int nextId = caseParamsDao.getCaseStatusDao().getNextCaseId();

        if (nextId < 0) {
            return HttpResponse.noContent();
        }

        return fromCaseId(nextId);
    }

    @Get(value = "/prev", produces = APPLICATION_JSON)
    public HttpResponse<String> getPrev() {
        int prevId = caseParamsDao.getCaseStatusDao().getPrevCaseId();

        if (prevId < 0) {
            return HttpResponse.noContent();
        }

        return fromCaseId(prevId);
    }

    @Get(value = "/current", produces = APPLICATION_JSON)
    public HttpResponse<String> getCurrent(int picoId) {
        int currentId = caseParamsDao.getCaseStatusDao().getCurrentCaseId();

        if (currentId < 0) {
            return HttpResponse.noContent();
        }

        return fromCaseId(currentId);
    }
}
