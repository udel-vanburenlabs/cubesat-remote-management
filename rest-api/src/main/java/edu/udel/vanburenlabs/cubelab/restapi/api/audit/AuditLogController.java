/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.audit;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabAuditEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabHubEventTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabPicoEventTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabAuditEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabHubEventTypesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabPicoEventTypesEntity;
import edu.udel.vanburenlabs.cubelab.restapi.dao.AuditHubEventTypesDao;
import edu.udel.vanburenlabs.cubelab.restapi.dao.AuditLogDao;
import edu.udel.vanburenlabs.cubelab.restapi.dao.AuditPicoEventTypesDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.time.LocalDateTime;

import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;

@Slf4j
@Controller("/api/audit")
public class AuditLogController {
    @Inject
    private AuditLogDao auditLogDao;

    @Inject
    private AuditPicoEventTypesDao auditPicoEventTypesDao;

    @Inject
    private AuditHubEventTypesDao auditHubEventTypesDao;

    @Get(value = "/picoTypes", produces = APPLICATION_JSON)
    public String allPicoTypes() {
        return toJsonString(auditPicoEventTypesDao.getAll());
    }

    @Get(value = "/picoTypes/{typeId}", produces = APPLICATION_JSON)
    public String picoTypeById(int typeId) {
        return toJsonString(auditPicoEventTypesDao.getByTypeId(typeId));
    }

    @Get(value = "/picoTypes/name/{type}", produces = APPLICATION_JSON)
    public String picoTypeByType(String type) {
        return toJsonString(auditPicoEventTypesDao.getByTypeName(type));
    }

    @Post(value = "/picoTypes", consumes = APPLICATION_JSON)
    public HttpResponse<Void> postPicoType(@Body String json) {
        CubelabPicoEventTypesEntity entity = auditPicoEventTypesDao.postType(fromJsonString(json, CubelabPicoEventTypesEntityDto.class));

        return HttpResponse.created(URI.create("/api/audit/picoTypes/" + entity.getPicoEventTypeId()));
    }

    @Get(value = "/hubTypes", produces = APPLICATION_JSON)
    public String allHubTypes() {
        return toJsonString(auditHubEventTypesDao.getAll().toList());
    }

    @Get(value = "/hubTypes/{typeId}", produces = APPLICATION_JSON)
    public String hubTypeById(int typeId) {
        return toJsonString(auditHubEventTypesDao.getByTypeId(typeId));
    }

    @Get(value = "/hubTypes/name/{type}", produces = APPLICATION_JSON)
    public String hubTypeByType(String type) {
        return toJsonString(auditHubEventTypesDao.getByTypeName(type));
    }

    @Post(value = "/hubTypes", consumes = APPLICATION_JSON)
    public HttpResponse<Void> postHubType(@Body String json) {
        CubelabHubEventTypesEntity entity = auditHubEventTypesDao.postType(fromJsonString(json, CubelabHubEventTypesEntityDto.class));

        return HttpResponse.created(URI.create("/api/audit/hubTypes/" + entity.getHubEventTypeId()));
    }

    @Get(produces = APPLICATION_JSON)
    public String all() {
        return toJsonString(auditLogDao.getAllLogEntries());
    }

    @Get(value = "/{eventId}", produces = APPLICATION_JSON)
    public String byEventId(int eventId) {
        return toJsonString(auditLogDao.getByEventId(eventId));
    }

    @Get(value = "/timestamp/{start}/to/{end}", produces = APPLICATION_JSON)
    public String byTimestamp(String start, String end) {
        return toJsonString(auditLogDao.getByTimestamp(LocalDateTime.parse(start), LocalDateTime.parse(end)));
    }

    @Get(value = "/hubTypeId/{typeId}", produces = APPLICATION_JSON)
    public String byHubEventTypeId(int typeId) {
        return toJsonString(auditLogDao.getByHubEventTypeId(typeId));
    }

    @Get(value = "/picoTypeId/{typeId}", produces = APPLICATION_JSON)
    public String byPicoEventTypeId(int typeId) {
        return toJsonString(auditLogDao.getByPicoEventTypeId(typeId));
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<Void> postEvent(@Body String json) {
        CubelabAuditEntity entity = auditLogDao.postLogEvent(fromJsonString(json, CubelabAuditEntityDto.class));

        return HttpResponse.created(URI.create("/api/audit/" + entity.getEventId()));
    }
}
