/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabAuditEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabAuditEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CubelabAuditMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class AuditLogDao {
    private final Query<CubelabAuditEntity> allLogEntries;
    private final Query<CubelabAuditEntity> logEntryByEventId;
    private final Query<CubelabAuditEntity> logEntriesByTimestamp;
    private final Query<CubelabAuditEntity> logEntriesByHubEventId;
    private final Query<CubelabAuditEntity> logEntriesByPicoEventId;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public AuditLogDao() {
        allLogEntries = session.createQuery("from CubelabAuditEntity", CubelabAuditEntity.class);
        logEntryByEventId = session.createQuery("from CubelabAuditEntity where eventId = ?1", CubelabAuditEntity.class);
        logEntriesByTimestamp = session.createQuery("from CubelabAuditEntity where timestamp between ?1 and ?2", CubelabAuditEntity.class);
        logEntriesByHubEventId = session.createQuery("from CubelabAuditEntity where hubEventTypeId = ?1", CubelabAuditEntity.class);
        logEntriesByPicoEventId = session.createQuery("from CubelabAuditEntity where picoEventTypeId = ?1", CubelabAuditEntity.class);
    }

    public Stream<CubelabAuditEntityDto> getAllLogEntries() {
        return allLogEntries
            .stream()
            .map(instance::entityToDTO);
    }

    @Nullable
    public CubelabAuditEntityDto getByEventId(int eventId) {
        return logEntryByEventId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public Stream<CubelabAuditEntityDto> getByTimestamp(LocalDateTime start, LocalDateTime end) {
        logEntriesByTimestamp.setParameter(1, start);
        logEntriesByTimestamp.setParameter(2, end);

        return logEntriesByTimestamp
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<CubelabAuditEntityDto> getByHubEventTypeId(int hubEventTypeId) {
        logEntriesByHubEventId.setParameter(1, hubEventTypeId);

        return logEntriesByHubEventId
            .stream()
            .map(instance::entityToDTO);
    }

    public Stream<CubelabAuditEntityDto> getByPicoEventTypeId(int picoEventTypeId) {
        logEntriesByPicoEventId.setParameter(1, picoEventTypeId);

        return logEntriesByPicoEventId
            .stream()
            .map(instance::entityToDTO);
    }

    public CubelabAuditEntity postLogEvent(CubelabAuditEntityDto dto) {
        CubelabAuditEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }
}
