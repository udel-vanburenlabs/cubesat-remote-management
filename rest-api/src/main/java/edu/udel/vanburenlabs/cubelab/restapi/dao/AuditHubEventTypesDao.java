/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.dao;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.CubelabHubEventTypesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.CubelabHubEventTypesEntity;
import edu.udel.vanburenlabs.cubelab.restapi.config.HibernateConfigurator;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;

import java.util.stream.Stream;

import static edu.udel.vanburenlabs.cubelab.datamodels.mappers.CubelabHubEventTypesMapper.instance;
import static edu.udel.vanburenlabs.cubelab.restapi.util.TransactionUtils.inTransaction;

@Slf4j
@Singleton
public class AuditHubEventTypesDao {

    private final Query<CubelabHubEventTypesEntity> allEventTypes;
    private final Query<CubelabHubEventTypesEntity> byTypeId;
    private final Query<CubelabHubEventTypesEntity> byTypeName;

    private final StatelessSession session = HibernateConfigurator.getSession();

    public AuditHubEventTypesDao() {
        allEventTypes = session.createQuery("from CubelabHubEventTypesEntity", CubelabHubEventTypesEntity.class);
        byTypeId = session.createQuery("from CubelabHubEventTypesEntity where hubEventTypeId = ?1", CubelabHubEventTypesEntity.class);
        byTypeName = session.createQuery("from CubelabHubEventTypesEntity where hubEventType = ?1", CubelabHubEventTypesEntity.class);
    }

    public Stream<CubelabHubEventTypesEntityDto> getAll() {
        return allEventTypes
            .stream()
            .map(instance::entityToDTO);
    }

    public CubelabHubEventTypesEntityDto getByTypeId(int typeId) {
        byTypeId.setParameter(1, typeId);

        return byTypeId
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CubelabHubEventTypesEntityDto getByTypeName(String name) {
        byTypeName.setParameter(1, name);

        return byTypeName
            .stream()
            .map(instance::entityToDTO)
            .findFirst()
            .orElse(null);
    }

    public CubelabHubEventTypesEntity postType(CubelabHubEventTypesEntityDto dto) {
        CubelabHubEventTypesEntity entity = instance.DTOToEntity(dto);
        log.debug(dto.toString());
        inTransaction(session, () -> session.insert(entity));
        return entity;
    }
}
