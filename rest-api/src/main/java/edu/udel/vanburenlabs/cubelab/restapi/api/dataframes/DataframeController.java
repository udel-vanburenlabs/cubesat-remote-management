/*
 * MIT License
 *
 * Copyright (c) 2023 Galen Nare
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package edu.udel.vanburenlabs.cubelab.restapi.api.dataframes;

import edu.udel.vanburenlabs.cubelab.datamodels.dto.PicoDataframesEntityDto;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.MessagesEntity;
import edu.udel.vanburenlabs.cubelab.datamodels.entity.PicoDataframesEntity;
import edu.udel.vanburenlabs.cubelab.restapi.dao.DataFrameDao;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;

import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.fromJsonString;
import static edu.udel.vanburenlabs.cubelab.restapi.util.JsonUtils.toJsonString;
import static io.micronaut.http.MediaType.APPLICATION_JSON;
import static io.micronaut.http.MediaType.TEXT_PLAIN;

@Slf4j
@Controller("/api/dataframe")
public class DataframeController {

    @Inject
    private DataFrameDao dataFrameDao;

    @Get(produces = APPLICATION_JSON)
    public String all() {
        return toJsonString(dataFrameDao.getDataFrames().toList());
    }

    @Get(value = "/recent/{n}", produces = APPLICATION_JSON)
    public String mostRecent(int n) {
        return toJsonString(dataFrameDao.getDataFrames(n).toList());
    }

    @Get(value = "/mcu/{picoId}/recent/{n}", produces = APPLICATION_JSON)
    public String mostRecent(int picoId, int n) {
        return toJsonString(dataFrameDao.getDataFrames(picoId, n).toList());
    }

    @Get(value = "/timestamp/{start}/to/{end}", produces = APPLICATION_JSON)
    public String betweenTimestamps(long start, long end) {
        LocalDateTime startDt = LocalDateTime.from(Instant.ofEpochMilli(start));
        LocalDateTime endDt = LocalDateTime.from(Instant.ofEpochMilli(end));
        return toJsonString(dataFrameDao.getDataFrames(startDt, endDt).toList());
    }

    @Get(value = "/mcu/{picoId}/timestamp/{timestamp}", produces = APPLICATION_JSON)
    public String fromPicoIdAndTimestamp(int picoId, long timestamp) {
        LocalDateTime dt = LocalDateTime.from(Instant.ofEpochMilli(timestamp));
        return toJsonString(Collections.singletonList(dataFrameDao.getDataFrame(picoId, dt)));
    }

    @Get(value = "/mcu/{picoId}/timestamp/{start}/to/{end}", produces = APPLICATION_JSON)
    public String fromPicoIdAndBetweenTimestamp(int picoId, long start, long end) {
        LocalDateTime startDt = LocalDateTime.from(Instant.ofEpochMilli(start));
        LocalDateTime endDt = LocalDateTime.from(Instant.ofEpochMilli(end));
        return toJsonString(dataFrameDao.getDataFrames(picoId, startDt, endDt).toList());
    }

    @Get(value = "/id/{id}", produces = APPLICATION_JSON)
    public String fromFrameId(int id) {
        return toJsonString(Collections.singletonList(dataFrameDao.getDataFrame(id)));
    }

    @Get(value = "/id/{id}/message", produces = TEXT_PLAIN)
    public String messageFromFrameId(int id) {
        return dataFrameDao.getMessageForFrameId(id);
    }

    @Post(consumes = APPLICATION_JSON)
    public HttpResponse<String> postDataFrame(@Body String json) {
        PicoDataframesEntityDto dto = fromJsonString(json, PicoDataframesEntityDto.class);
        PicoDataframesEntity entity = dataFrameDao.putDataframe(dto);
        return HttpResponse.created(HttpResponse.uri("/api/dataframe/id/" + entity.getFrameId()));
    }

    @Post(value = "/message/", consumes = TEXT_PLAIN)
    public HttpResponse<String> postMessage(@Body String message) {
        MessagesEntity entity = dataFrameDao.putMessage(message);
        return HttpResponse.created(HttpResponse.uri("/api/dataframe/message/" + entity.getMessageId()));
    }

    @Get(value = "/message/{id}", produces = TEXT_PLAIN)
    public String messageById(int id) {
        return dataFrameDao.getMessageById(id);
    }
}
