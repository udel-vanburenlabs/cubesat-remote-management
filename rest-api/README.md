# Cubelab Rest API

### Authors:

Galen Nare

---

A Micronaut application that provides HTTP endpoints for exchanging
data between our various Cubelab apps, and hardware devices
(i.e. Pico MCU and FLIR camera controllers). 

### Top-level dependencies:
 
 - _Micronaut_ - HTTP framework for streamlining server development; has
automatic parallelization, memory management, and session management. 
Pretty lightweight compared to competing frameworks like Spring.
 - _Hibernate_ - Manages data persistence and standardizes database connections;
allows all database interactions to happen directly through Java code 
(direct SQL commands not required).
 - _PostgreSQL_ - Driver to interact with a Postgres database. Used by Hibernate.
 - _Gson_ - tools for encoding/decoding JSON data. We send JSON responses from most
of the endpoints in this API.
 - _Jakarta Enterprise_ - Used for dependency injection and to ensure the application
is set up properly.
 - _MapStruct_ - Used by the data model classes to easily map between Hibernate Entity
and DTO objects.
 - _SLF4J and Logback_ - Logging interface to enable pretty-printed messages and various log levels.

### How to run/install:

 - Clone the repo using `git clone`
 - Ensure Maven is installed (version 3.8 is preferred).
 - Ensure the Pico Data models project has already been installed (../datamodels).
 - To run the app, run `mvn clean mn:run`.
 - To install to the local repo and build a JAR, run `mvn clean install`

### Connection info:

 - Server runs locally on port `8080`. It is not recommended to port forward or allow this
through a system firewall as it is in HTTP cleartext.
 - Ensure a local Postgres server is running: `sudo systemctl start postgresql`. Failing to do
so will cause all requests to return a 500 status code.
   - Because this server is only for local use, it uses the default username and password for postgres 
(`postgres`/`postgres`). This can be changed in `src/main/resources/hibernate.cfg.xml`.

### License:

MIT

Feel free to use this code for whatever you want (credit appreciated but not required), 
but if you claim you wrote it I will publicly shame you :)